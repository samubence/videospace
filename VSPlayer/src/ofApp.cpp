#include "ofApp.h"
#include "ofxXmlSettings.h"
#include "SceneContent.h"
#include "xioAllElements.h"

/*
 TODO:

 border, az area-k szelen, hogy ne ugralhasson ide/oda

 hang nem all le, ha kimegy

- forward OSC
 */

string ofApp::currentFolder = "";
string ofApp::logText = "";

ofApp::ofApp()
{

}
//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetVerticalSync(true);
    ofEnableAlphaBlending();
    //ofHideCursor();

    titleAlpha = 1;
    titleInterval = 5;

    port = 12345;
    sceneInterval = 60 * 5;
    sceneStarted = -1;

    scene = NULL;
    cContent = NULL;
    wScene = NULL;

    isUserOut = true;

    loadSettings();

    oscReceiver.setup(port);


    ofDirectory dir;
    int n = dir.listDir("scenes");
    for (int i = 0; i < n; i++)
    {
        string sceneFolder = dir.getPath(i);
        printf("%s\n", sceneFolder.c_str());
        sceneList.push_back(sceneFolder);
    }

    if (sceneList.size() == 0)
    {
        scene = new BaseScene();
        doLoadNextScene = false;
        printf("Empty scene created\n");
        currentFolder = "";
        loadScene();
    }
    else
    {
        sceneIndex = sceneList.begin();
        doLoadNextScene = true;
        ofHideCursor();
        ofToggleFullscreen();
        xio.show = false;
    }

    topView.set(0, ofGetHeight() - 200, 200, 200);
    xio.setStyle(new xioDefaultStyleAlpha());
    xio.useStencil = false;

    xioWindow * w = xio.createWindow("create / edit content", 0, 0, 200, 500, false, true, true);
    w->addGroup("add");
    w->addElement(this, new xio_PushButton("add movie"), XIO_ADD_MOVIE);
    w->addElement(this, new xio_PushButton("add seqence"), XIO_ADD_SEQUENCE);
    w->addGroup("edit");
    w->addElement(this, new xio_PushButton("next content"), XIO_NEXT_CONTENT);
    w->addElement(this, new xio_PushButton("delete content"), XIO_DELETE);
    w->addElement(this, new xio_PushButton("save scene"), XIO_SAVE);

    doLoadMovie = doLoadSequence = false;
    doRefreshGUI = false;
}

//--------------------------------------------------------------
void ofApp::update()
{
    //ofSetWindowTitle(ofToString(ofGetFrameRate()));

    while( oscReceiver.hasWaitingMessages() )
    {
        ofxOscMessage m;
        oscReceiver.getNextMessage( &m );

        if ( strcmp( m.getAddress().c_str(), "/userPos" ) == 0 )
        {
            float x = m.getArgAsFloat( 0 );
            float y = m.getArgAsFloat( 1 );
            float z = m.getArgAsFloat( 2 );
            userPos.set(x, y, z);
            isUserOut = false;
        }
        else if (strcmp( m.getAddress().c_str(), "/userOut" ) == 0 )
        {
            isUserOut = true;
        }
    }

    if (scene)
    {
        scene->setUserPos(userPos);
        scene->update();
    }

    if (titleStarted != -1)
    {
        titleAlpha = powf(ofMap(ofGetElapsedTimef(), titleStarted, titleStarted + titleInterval, 0, 1, true), 3);
        titleAlpha = 1 - titleAlpha;
    }
    if (doLoadMovie)
    {
        addMovie();
        doLoadMovie = false;
    }

    if (doLoadSequence)
    {
        addSequence();
        doLoadSequence = false;
    }

    if (doRefreshGUI)
    {
        doRefreshGUI = false;
        refreshGUI();
    }
    if (doLoadNextScene)
    {
        loadNextScene();
        sceneStarted = ofGetElapsedTimef();
        doLoadNextScene = false;
    }

    if (sceneStarted != -1 && isUserOut && ofGetElapsedTimef() - sceneStarted > sceneInterval)
    {
        doLoadNextScene = true;
        isUserOut = false;
    }

}

//--------------------------------------------------------------
void ofApp::draw()
{

    ofBackgroundGradient(ofColor(20), ofColor(60), OF_GRADIENT_LINEAR);

    if (scene)
    {
        scene->draw();

        ofPushMatrix();
        ofTranslate(topView.x + topView.width / 2., topView.y, 0);
        ofScale(topView.width / (float)space.width, topView.height / (float)space.width, 1);

        ofSetColor(50, 100);
        ofFill();

        ofRect(-space.width / 2., 0, space.width, space.width);

        scene->drawContentAreas(xio.show ? cContent : NULL);

        ofSetColor(255, 100);
        ofFill();
        ofCircle(userPos.x, userPos.z, 50);

        ofSetColor(255, 150);
        ofNoFill();
        ofCircle(userPos.x, userPos.z, 50);

        ofSetColor(255, 200);
        ofFill();
        ofCircle(userPos.x, userPos.z, 20);

        ofSetColor(255);

        ofDrawBitmapString("  x: " + ofToString((int)userPos.x) + " mm\n  y: " + ofToString((int)userPos.y) + " mm\n  z: " + ofToString((int)userPos.z) + " mm", userPos.x, userPos.z);

        ofNoFill();
        ofRect(-space.width / 2., 0, space.width, space.width);

        ofPopMatrix();

        if (title.isAllocated())
        {
            ofSetColor(255, titleAlpha * 255);
            title.draw(0, ofGetHeight() - title.getHeight(), 0);
        }

    }

    if (doLoadNextScene)
    {
        ofSetColor(0, 100); ofFill();
        ofRect(0, ofGetHeight() / 2 - 60, ofGetWidth(), 100);
        ofDrawBitmapStringHighlight("LOADING...", ofGetWidth() / 2, ofGetHeight() / 2);
    }

    if (xio.show)
    {
        xio.draw();
        if (logText.length()) ofDrawBitmapStringHighlight(logText, ofGetWidth() / 2. - 200, ofGetHeight() - 20);
    }
    //ofSetColor(0, 255, 255);ofDrawBitmapString(ofToString(ofGetFrameRate()), ofGetWidth() - 100, 30);
}

void ofApp::xioEvent(xioEventArgs &e)
{
    logText = "";

    if (e.id == XIO_ADD_MOVIE)
    {
        doLoadMovie = true;
    }
    if (e.id == XIO_ADD_SEQUENCE)
    {
        doLoadSequence = true;
    }
    if (e.id == XIO_NEXT_CONTENT)
    {
        nextContent();
    }
    if (e.id == XIO_AREA_TYPE)
    {
        if (e.values[0]) cContent->area->type = NiArea::CONTENT_PLAY;
        if (e.values[1]) cContent->area->type = NiArea::CONTENT_LOOP;
        if (e.values[2]) cContent->area->type = NiArea::CONTENT_X_2_PERCENT;
        if (e.values[3]) cContent->area->type = NiArea::CONTENT_Y_2_PERCENT;
        if (e.values[4]) cContent->area->type = NiArea::CONTENT_Z_2_PERCENT;
    }
    if (e.id == XIO_SAVE)
    {
        saveScene();
    }
    if (e.id == XIO_DELETE)
    {
        deleteContent();
    }
}
void ofApp::refreshGUI()
{
    if (wScene) xio.destroyWindow(wScene);
    if (cContent)
    {
        wScene = xio.createWindow(cContent->cPlayer->fileName, ofGetWidth() - 200, 0, 200, ofGetHeight(), false, true, false);

        wScene->addGroup("content");
        wScene->addElement(this, new xio_NumberInput("center x", -2000, 2000, &cContent->cPlayer->center.x));
        wScene->addElement(this, new xio_NumberInput("center y", -2000, 2000, &cContent->cPlayer->center.y));
        wScene->addElement(this, new xio_NumberInput("center z", -2000, 2000, &cContent->cPlayer->center.z));
        wScene->addElement(this, new xio_NumberInput("width", 1, 1280, &cContent->cPlayer->width));
        wScene->addElement(this, new xio_NumberInput("height", 1, 720, &cContent->cPlayer->height));
        wScene->addElement(this, new xio_NumberInput("rotation x", -180, 180, &cContent->cPlayer->rotation.x));
        wScene->addElement(this, new xio_NumberInput("rotation y", -180, 180, &cContent->cPlayer->rotation.y));
        wScene->addElement(this, new xio_NumberInput("rotation z", -180, 180, &cContent->cPlayer->rotation.z));
        wScene->addGroup("area");
        wScene->addElement(this, new xio_NumberInput("x", -1000, 1000, &cContent->area->area.x));
        wScene->addElement(this, new xio_NumberInput("y", 0, 2000, &cContent->area->area.y));
        wScene->addElement(this, new xio_NumberInput("width", 0, 2000, &cContent->area->area.width));
        wScene->addElement(this, new xio_NumberInput("height", 0, 2000, &cContent->area->area.height));
        wScene->addElement(this, new xio_ToggleList("PLAY LOOP X_CONTROL Y_CONTROL Z_CONTROL"), XIO_AREA_TYPE);
        wScene->addElement(this, new xio_ToggleButton("hide when inactive", &cContent->area->hideWhenInactive));
        wScene->addElement(this, new xio_NumberInput("fade", 0, 1000, &cContent->area->fadeInterval));
        wScene->addGroup("camera");
        wScene->addElement(this, new xio_NumberInput("min x", -2000, 2000, &scene->camMinX));
        wScene->addElement(this, new xio_NumberInput("max x", -2000, 2000, &scene->camMaxX));
        wScene->addElement(this, new xio_NumberInput("min y", -2000, 2000, &scene->camMinY));
        wScene->addElement(this, new xio_NumberInput("max y", -2000, 2000, &scene->camMaxY));
        wScene->addElement(this, new xio_NumberInput("min z", -2000, 2000, &scene->camMinZ));
        wScene->addElement(this, new xio_NumberInput("max z", -2000, 2000, &scene->camMaxZ));
        wScene->addElement(this, new xio_NumberInput("fov", 20, 90, &scene->camFov));
    }
}

void ofApp::loadSettings()
{
    ofxXmlSettings xml;
    if (xml.loadFile("settings.xml"))
    {
        float x = xml.getValue("settings:space:x", space.x, 0);
        float y = xml.getValue("settings:space:y", space.y, 0);
        float w = xml.getValue("settings:space:width", space.width, 0);
        float h = xml.getValue("settings:space:height", space.height, 0);
        space.set(x, y, w, h);

        port = xml.getValue("settings:osc:port", port, 0);

        sceneInterval = xml.getValue("settings::scene:sceneInterval", sceneInterval, 0);
        titleInterval = xml.getValue("settings::scene:titleInterval", titleInterval, 0);
    }
}

void ofApp::saveScene()
{
    if (scene)
    {
        ofxXmlSettings xml;
        int sceneTag = xml.addTag("scene");
        xml.pushTag("scene", sceneTag);

        int cameraTag = xml.addTag("camera");
        xml.pushTag("camera", cameraTag);
        xml.addValue("minX", scene->camMinX);
        xml.addValue("maxX", scene->camMaxX);
        xml.addValue("minY", scene->camMinY);
        xml.addValue("maxY", scene->camMaxY);
        xml.addValue("minZ", scene->camMinZ);
        xml.addValue("maxZ", scene->camMaxZ);
        xml.addValue("fov", scene->camFov);
        xml.popTag();

        for (int i = 0; i < scene->content.size(); i++)
        {
            SceneContent * c = scene->content[i];
            int cTag = xml.addTag("content");
            xml.pushTag("content", cTag);

            xml.addValue("type", c->contentType);
            int sTag = xml.addTag("contentPlayer");

            xml.pushTag("contentPlayer", sTag);
            xml.addValue("file", c->cPlayer->fileName);
            xml.addValue("centerX", c->cPlayer->center.x);
            xml.addValue("centerY", c->cPlayer->center.y);
            xml.addValue("centerZ", c->cPlayer->center.z);
            xml.addValue("width", c->cPlayer->width);
            xml.addValue("height", c->cPlayer->height);
            xml.addValue("rotationX", c->cPlayer->rotation.x);
            xml.addValue("rotationY", c->cPlayer->rotation.y);
            xml.addValue("rotationZ", c->cPlayer->rotation.z);

            xml.popTag();

            int aTag = xml.addTag("area");
            xml.pushTag("area", aTag);
            xml.addValue("x", c->area->area.x);
            xml.addValue("y", c->area->area.y);
            xml.addValue("width", c->area->area.width);
            xml.addValue("height", c->area->area.height);
            xml.addValue("type", c->area->type);
            xml.addValue("fadeInterval", c->area->fadeInterval);
            xml.addValue("hideWhenInactive", c->area->hideWhenInactive);
            xml.popTag();

            xml.popTag();
        }

        xml.popTag();
        xml.save("scene.xml");
    }
}

void ofApp::loadScene()
{
    ofxXmlSettings xml;

    printf("try loading scene: %s\n", currentFolder.c_str());
    if (xml.load(currentFolder + "scene.xml"))
    {
        printf("loading scene...");


        if (scene) delete scene;
        scene = new BaseScene();

        xml.pushTag("scene");

        int contentPlayerType = 0;

        xml.pushTag("camera");
        scene->camMinX = xml.getValue("minX", 0.f);
        scene->camMaxX = xml.getValue("maxX", 0.f);
        scene->camMinY = xml.getValue("minY", 0.f);
        scene->camMaxY = xml.getValue("maxY", 0.f);
        scene->camMinZ = xml.getValue("minZ", 0.f);
        scene->camMaxZ = xml.getValue("maxZ", 0.f);
        scene->camFov = xml.getValue("fov", 0.f);
        xml.popTag();

        int n = xml.getNumTags("content");
        for (int i = 0; i < n; i++)
        {
            xml.pushTag("content", i);
            contentPlayerType = xml.getValue("type", 0);
            string file;
            ofVec3f center, rotation;
            float width, height;
            ofRectangle area;
            int areaType;
            int fadeInterval;
            bool hideWhenInactive;

            xml.pushTag("contentPlayer");
            file = xml.getValue("file", "");
            center.x = xml.getValue("centerX", 0.f);
            center.y = xml.getValue("centerY", 0.f);
            center.z = xml.getValue("centerZ", 0.f);
            width = xml.getValue("width", 0.f);
            height = xml.getValue("height", 0.f);
            rotation.x = xml.getValue("rotationX", 0.f);
            rotation.y = xml.getValue("rotationY", 0.f);
            rotation.z = xml.getValue("rotationZ", 0.f);
            xml.popTag();

            xml.pushTag("area");
            areaType = xml.getValue("type", 0);
            area.x = xml.getValue("x", 0.f);
            area.y = xml.getValue("y", 0.f);
            area.width = xml.getValue("width", 0.f);
            area.height = xml.getValue("height", 0.f);
            fadeInterval = xml.getValue("fadeInterval", 0);
            hideWhenInactive = xml.getValue("hideWhenInactive", 0);
            xml.popTag();

            xml.popTag();

            SceneContent * content = new SceneContent(file, contentPlayerType, center, width, height, rotation, area, areaType, hideWhenInactive, fadeInterval, 1);
            scene->addContent(content);
            cContent = content;
        }

        xml.popTag();

        printf("  DONE\n");

        doRefreshGUI = true;
    }
}

void ofApp::loadNextScene()
{
    if (sceneList.size() == 0) return;

    sceneIndex++;

    if (sceneIndex >= sceneList.end()) sceneIndex = sceneList.begin();

    currentFolder = *sceneIndex + "/";

    title.loadImage(currentFolder + "/title.png");

    loadScene();
    titleStarted = ofGetElapsedTimef();
}

void ofApp::addMovie()
{
    ofFileDialogResult fd = ofSystemLoadDialog();
    if (fd.getName() != "")
    {
        printf("Loading: %s\n", fd.getPath().c_str());
        ofFile f(fd.getName());
        if (f.isDirectory())
        {

            printf("Opening Sequence: %s\n", fd.getName().c_str());

            cContent = new SceneContent(fd.getName(), SceneContent::CONTENT_SEQUENCE, ofVec3f(0, 0, 0), 1280, 720, ofVec3f(), ofRectangle(-space.width / 2., 0, space.width, space.width), NiArea::CONTENT_LOOP, true, 0, 1);
            scene->addContent(cContent);
        }
        else
        {
            printf("Opening Movie: %s\n", fd.getName().c_str());
            cContent = new SceneContent(fd.getName(), SceneContent::CONTENT_MOVIE, ofVec3f(0, 0, 0), 1280, 720, ofVec3f(), ofRectangle(-space.width / 2., 0, space.width, space.width), NiArea::CONTENT_LOOP, true, 0, 1);
            scene->addContent(cContent);
            printf("Scene added\n");
        }

        doRefreshGUI = true;
    }
}

void ofApp::addSequence()
{
    ofFileDialogResult fd = ofSystemLoadDialog("", true, ofToDataPath(""));
    if (fd.getName() != "")
    {
        printf("Loading: %s\n", fd.getPath().c_str());
        ofFile f(fd.getName());
        if (f.isDirectory())
        {

            printf("Opening Sequence: %s\n", fd.getName().c_str());

            cContent = new SceneContent(fd.getName(), SceneContent::CONTENT_SEQUENCE, ofVec3f(0, 0, 0), 1280, 720, ofVec3f(), ofRectangle(-space.width / 2., 0, space.width, space.width), NiArea::CONTENT_LOOP, true, 0, 1);
            scene->addContent(cContent);
        }
        else
        {
            printf("Opening Movie: %s\n", fd.getName().c_str());
            cContent = new SceneContent(fd.getName(), SceneContent::CONTENT_MOVIE, ofVec3f(0, 0, 0), 1280, 720, ofVec3f(), ofRectangle(-space.width / 2., 0, space.width, space.width), NiArea::CONTENT_LOOP, true, 0, 1);
            scene->addContent(cContent);
            printf("Scene added\n");
        }

        doRefreshGUI = true;
    }
}

void ofApp::nextContent()
{
    if (scene)
    {
        for (int i = 0; i < scene->content.size(); i++)
        {
            if (scene->content[i] == cContent)
            {
                if (i < scene->content.size() - 1)
                    cContent = scene->content[i + 1];
                else
                    cContent = scene->content[0];

                doRefreshGUI = true;
                return;
            }
        }
    }
}

void ofApp::deleteContent()
{
    if (scene)
    {
        scene->removeContent(cContent);
        delete cContent;
        cContent = NULL;
        if (scene->content.size() > 0) cContent = scene->content[0];

        doRefreshGUI = true;
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if (key == ' ') if (sceneList.size() > 0) doLoadNextScene = true;
    if (key == 'x') xio.toggleVisibility();
    if (key == 'f') ofToggleFullscreen();

    xio.show ? ofShowCursor() : ofHideCursor();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y )
{

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
    if (topView.inside(x, y))
    {
        userPos.set(ofMap(x, topView.x, topView.x + topView.width, space.x, space.x + space.width, true),
                    0,
                    ofMap(y, topView.y, topView.y + topView.height, 0, space.width, true));
    }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
    topView.set(0, ofGetHeight() - 200, 200, 200);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg)
{

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{

}
