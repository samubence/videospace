//
//  SceneContent.cpp
//  VSPlayer
//
//  Created by bence samu on 25/01/15.
//
//

#include "SceneContent.h"
#include "ofApp.h"

void ContentMoviePlayer::setup(string _fileName, ofVec3f _center, float _width, float _height, ofVec3f _rotation)
{
    fileName = _fileName;
    if (!vPlayer.loadMovie(ofApp::currentFolder + fileName))
    {
        ofApp::logText = "ERROR loading movie: " + fileName;
    }

    center = _center;
    width = _width;
    height = _height;
    rotation = _rotation;
    vPlayer.setPosition(0.1);
    vPlayer.update();
    vPlayer.setLoopState(OF_LOOP_NONE);
}

ContentMoviePlayer::~ContentMoviePlayer()
{
    vPlayer.close();
}

void ContentMoviePlayer::play(bool _looping)
{
    vPlayer.setLoopState(_looping ? OF_LOOP_NORMAL : OF_LOOP_NONE);
    if (!_looping) vPlayer.setPosition(0);
    vPlayer.play();
}

void ContentMoviePlayer::stop()
{
    vPlayer.stop();
}

void ContentMoviePlayer::update()
{
    vPlayer.update();
}

void ContentMoviePlayer::setPercent(float p)
{
    vPlayer.setPaused(true);
    vPlayer.setPosition(p);
    vPlayer.update();
}

ofTexture * ContentMoviePlayer::getTexture()
{
    return &vPlayer.getTextureReference();
}

void ContentMoviePlayer::setSpeed(float v)
{
    vPlayer.setSpeed(v);
}

void ContentMoviePlayer::draw(ofRectangle view)
{
    ofEnableDepthTest();
    getTexture()->draw(view);
    ofDisableDepthTest();
}


/////////////////////////////////////////////////////////////////////


void ContentSeqPlayer::setup(string dirName, ofVec3f _center, float _width, float _height, ofVec3f _rotation)
{
    fileName = dirName;
    center = _center;
    width = _width;
    height = _height;
    rotation = _rotation;

    ofDirectory dirList;
    int n = dirList.listDir(ofApp::currentFolder + dirName);
    ofImage img;
    for (int i = 0; i < n; i++)
    {
        img.loadImage(dirList.getPath(i));
        ofTexture * text = new ofTexture();
        text->allocate(img.width, img.height, img.type == OF_IMAGE_COLOR ? GL_RGB : GL_RGBA);
        text->loadData(img.getPixels(), img.width, img.height, img.type == OF_IMAGE_COLOR ? GL_RGB : GL_RGBA);
        frames.push_back(text);
        printf("%s\n", dirList.getPath(i).c_str());

    }
    currentFrame = 0;
    started = 0;
    playing = false;
    fps = 15;
};

ContentSeqPlayer::~ContentSeqPlayer()
{
    for (int i = 0; i < frames.size(); i++)
    {
        //printf("deleting %i\n", i);
        delete frames[i];
    }
    frames.clear();
}

void ContentSeqPlayer::play(bool _looping)
{
    playing = true;
    looping = _looping;
    started = ofGetElapsedTimeMillis();
};

void ContentSeqPlayer::setSpeed(float v)
{
    fps = 15 * v;
    // ??
}

void ContentSeqPlayer::setFPS(int _fps)
{
    fps = _fps;
}

void ContentSeqPlayer::update()
{
    if (playing)
    {
        currentFrame = ((ofGetElapsedTimeMillis() - started) / (1000 / fps));

        if (looping)
            currentFrame = currentFrame % frames.size();
        else if (currentFrame >= frames.size())
        {
            currentFrame = frames.size() - 1;
            playing = false;
        }
    }
}

void ContentSeqPlayer::setPercent(float p)
{
    p = ofClamp(p, 0, 1);
    currentFrame = p * (frames.size() - 1);
    playing = false;
}

ofTexture * ContentSeqPlayer::getTexture()
{
    return frames[currentFrame];
}

void ContentSeqPlayer::draw(ofRectangle view)
{
    ofEnableDepthTest();
    getTexture()->draw(view);
    ofDisableDepthTest();
}

int ContentSeqPlayer::getNumFrames()
{
    return frames.size();
}
