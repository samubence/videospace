#pragma once

#include "ofMain.h"
#include "BaseScene.h"
#include "ofxOscReceiver.h"
#include "XIO.h"

enum
{
    XIO_ADD_MOVIE,
    XIO_ADD_SEQUENCE,
    XIO_NEXT_CONTENT,
    XIO_AREA_TYPE,
    XIO_SAVE,
    XIO_DELETE
};

class BaseScene;
class SceneContent;

class ofApp : public ofBaseApp, xioListener
{
public:
    ofApp();
    void setup();
    void update();
    void draw();

    void keyPressed  (int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    void xioEvent(xioEventArgs & e);
    void refreshGUI();

    void loadSettings();
    
    void loadNextScene();
    void saveScene();
    void loadScene();
    
    void addMovie();
    void addSequence();
    void nextContent();
    void deleteContent();
    
    XIO xio;
    xioWindow * wScene;

    ofVec3f userPos;

    BaseScene * scene;
    SceneContent * cContent;

    ofRectangle topView;
    ofRectangle space;
    ofImage title;
    float titleAlpha;
    float titleStarted, titleInterval;

    ofxOscReceiver oscReceiver;
    int port;

    bool isUserOut;
    float sceneInterval, sceneStarted;

    vector<string> sceneList;
    vector<string>::iterator sceneIndex;

    static string currentFolder;
    static string logText;

    bool doLoadNextScene;
    bool doRefreshGUI;
    bool doLoadMovie, doLoadSequence;
};
