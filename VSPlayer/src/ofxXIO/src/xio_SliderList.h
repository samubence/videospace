#ifndef __xio_SliderList_h__
#define __xio_SliderList_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_SliderList : public xioBaseElement
{
    public:
        xio_SliderList(string _name, vector<string> _textArray, int defaultValue)
        {
            xioData.name = _name;
            textArray = _textArray;
            valueMin = 0;
            valueMax = textArray.size() - 1;
            valuePercent = ofMap(defaultValue, valueMin, valueMax, 0, 1);
            xioData.value = defaultValue;
            intPtr = NULL;
        };

        xio_SliderList(string _name, vector<string> _textArray, int * _intPtr)
        {
            xioData.name = _name;
            textArray = _textArray;
            valueMin = 0;
            valueMax = textArray.size() - 1;
            intPtr = _intPtr;
            xioData.value = *intPtr;
            valuePercent = ofMap(xioData.value, valueMin, valueMax, 0, 1);
        };

        ~xio_SliderList(){};

        void update()
        {

        };

        void draw()
        {
            float x = valuePercent * getWidth();

            parent->useElementFillColor();
            ofFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementStrokeColor();
            ofNoFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementFontColor();
            if (textArray.size() > 0)
                parent->drawString(xioData.name + " : " + textArray[(int)xioData.value], 0, 0, getWidth(), getHeight(), XIO_ALIGN_CENTER);
        };

        void mousePressed(float x, float y, int button)
        {
            if (textArray.size() == 0) return;
            valuePercent = x;
            xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            xioData.value = (int)xioData.value;
            if (intPtr) *intPtr = (int)xioData.value;
            notifyEvent();
            updateValues();
        };

        void updateValues()
        {
            if (intPtr) xioData.value = *intPtr;
            if (textArray.size() == 0) return;
            valuePercent = ofMap(xioData.value, valueMin, valueMax, 0, 1);

        };

        void mouseDragged(float x, float y, int button)
        {
            if (textArray.size() == 0) return;
            valuePercent = x;
            xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            xioData.value = (int)xioData.value;
            if (intPtr) *intPtr = (int)xioData.value;
            notifyEvent();
            updateValues();
        };

        float valuePercent;
        float valueMin, valueMax;
        int * intPtr;
        vector<string> textArray;

};

#endif

