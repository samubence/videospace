#ifndef __xio_ToggleList_h__
#define __xio_ToggleList_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_ToggleList : public xioBaseElement
{
    public:
        xio_ToggleList(string _names, bool _multipleSelection = false)
        {

            multipleSelection = _multipleSelection;
            selected = -1;
            setList(_names);
        };

        ~xio_ToggleList()
        {

        };

        void setList(string _names)
        {
            //printf("names: %s\n", _names.c_str());
            names.clear();
            xioData.name = _names;
            xioData.values.clear();
            string tmp;
            for (int i = 0; i < xioData.name.length(); i++)
            {
                if (xioData.name.at(i) == ' ')
                {
                    names.push_back(tmp);
                    xioData.values.push_back(0);
                    tmp.clear();
                }
                else
                {
                    tmp += xioData.name.at(i);
                    if (i == xioData.name.length() - 1)
                    {
                        names.push_back(tmp);
                        xioData.values.push_back(0);
                    }
                }
            }
            oneHeight = XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT;
            height = oneHeight * names.size();
        }

        void draw()
        {
            for (int i = 0; i < names.size(); i++)
            {
                if (i == selected)
                {
                    parent->useElementBgColor();
                    ofFill();
                    ofRect(0, i * oneHeight, width, oneHeight);
                }

                parent->useElementFontColor();
                parent->drawString(names[i], 0, i * oneHeight, width - 10, oneHeight, XIO_ALIGN_RIGHT);

                int h = oneHeight - 8;
                if (xioData.values[i])
                {
                    parent->useElementFillColor();
                    ofFill();
                    ofRect(4, i * oneHeight + 4, h, h);
                }
                parent->useElementStrokeColor();
                ofNoFill();
                ofRect(4, i * oneHeight + 4, h, h);
            }
        };

        void mouseMoved(float x, float y)
        {
            selected = (int)((names.size() + 0) * y);
        };

        void mouseMovedOut()
        {
            selected = -1;
        };

        void mousePressed(float x, float y, int button)
        {
            int n = (int)((names.size() + 0) * y);

            if (multipleSelection)
            {
                xioData.values[n] = !xioData.values[n];
            }
            else
            {
                for (int i = 0; i < xioData.values.size(); i++)
                {
                    if (i == n)
                    {
                        xioData.value = i;
                        xioData.values[i] = true;
                    }
                    else xioData.values[i] = false;
                }
            }

            notifyEvent();
        };

        void updateValues()
        {
            names.clear();
            string tmp;

            for (int i = 0; i < xioData.name.length(); i++)
            {
                if (xioData.name.at(i) == ' ')
                {
                    names.push_back(tmp);
                    tmp.clear();
                }
                else
                {
                    tmp += xioData.name.at(i);
                }
            }
            oneHeight = XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT;
            height = oneHeight * names.size();
        }

        int selected;
        bool multipleSelection;
        int oneHeight;
        vector<string> names;

};

#endif

