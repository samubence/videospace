#include "XIO.h"
#include "xioDefaultStyleAlpha.h"

int XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT = 40;
int XIO_DEF::XIO_WINDOW_TITLE_HEIGHT = 50;
int XIO_DEF::XIO_SLIDER_WIDTH = 0;
int XIO_DEF::XIO_SLIDER_HEIGHT = 25;
int XIO_DEF::XIO_GROUP_HEIGHT = 20;

XIO::XIO()
{
    ofAddListener(ofEvents().update, this, &XIO::update);

    ofAddListener(ofEvents().keyPressed, this, &XIO::keyPressed);
    ofAddListener(ofEvents().mouseMoved, this, &XIO::mouseMoved);
    ofAddListener(ofEvents().mousePressed, this, &XIO::mousePressed);
    ofAddListener(ofEvents().mouseDragged, this, &XIO::mouseDragged);
    ofAddListener(ofEvents().mouseReleased, this, &XIO::mouseReleased);

    style = new xioDefaultStyle();
    useStencil = true;
    windowCollision = false;

    windowDragging = NULL;
    windowSelected = NULL;
    alertWindow = NULL;
    connecting = NULL;
    selectedWindow = NULL;
    useKeyboardNavigation = false;

    xml = new ofxXmlSettings();



    show = true;
}

XIO::~XIO()
{

}

xioWindow * XIO::createWindow(string windowTitle, int posX, int posY, int width, int height, bool movable, bool isOpen, bool autoHeight)
{
    screenArea = ofRectangle(0, 0, ofGetWidth(), ofGetHeight());
    xioWindow * window = new xioWindow(windowTitle, posX, posY, width, height, movable, isOpen, autoHeight);
    window->setParent(this);
    windows.push_back(window);
    return window;
}

xioAlertWindow * XIO::createAlertWindow(xioListener * listenerClass)
{
    alertWindow = new xioAlertWindow(listenerClass);
    return alertWindow;
}

void XIO::destroyWindow(xioWindow * w)
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end();)
    {
        if ((*i) == w)
        {
            // TODO erase window elements from XIO element vector
            if (windowSelected == w) windowSelected = NULL;
            if (windowDragging == w) windowDragging = NULL;
            if (selectedWindow == w) selectedWindow = NULL;
            delete w;
            i = windows.erase(i);
            return;
        }
        i++;
    }
}

void XIO::update(ofEventArgs &e)
{
    update();
}

void XIO::update()
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end();i++)
    {
        (*i)->update();
    }
}

void XIO::draw()
{
    if (show)
    {
        for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end(); i++)
        {
            (*i)->draw();
        }

        drawConnectors();
    }
}

void XIO::drawConnectors()
{
    if (connecting != NULL)
    {
        ofPoint a, b;
        a.set(connecting->posX, connecting->posY + connecting->height / 2.);

        useWindowBgColor();
        ofFill();
        ofCircle(a.x, a.y, 6);
        ofCircle(mouse.x, mouse.y, 6);
        glLineWidth(6);
        ofLine(a.x, a.y, mouse.x, mouse.y);
        useElementStrokeColor();
        glLineWidth(3);
        ofLine(a.x, a.y, mouse.x, mouse.y);
        glLineWidth(1);
    }

    for (vector<xioWindow*>::iterator w = windows.begin(); w != windows.end(); w++)
    {
        for (vector<xioBaseElement*>::iterator e = (*w)->elements.begin(); e != (*w)->elements.end(); e++)
        {
            for (vector<xioBaseElement*>::iterator c = (*e)->connectedElements.begin(); c != (*e)->connectedElements.end(); c++)
            {
                ofPoint a, b;
                a.set((*e)->posX, (*e)->posY + (*e)->height / 2.);
                b.set((*c)->posX + (*c)->width, (*c)->posY + (*c)->height / 2.);

                useWindowBgColor();
                ofFill();
                ofCircle(a.x, a.y, 6);
                ofCircle(b.x, b.y, 6);
                glLineWidth(6);
                ofLine(a.x, a.y, b.x, b.y);
                useElementStrokeColor();
                glLineWidth(3);
                ofLine(a.x, a.y, b.x, b.y);
                glLineWidth(1);
            }
        }
    }
}

bool XIO::isMouseOver()
{
    if (!show) return false;
    return (getWindowUnderPoint(mouse.x, mouse.y) != NULL);
}

bool XIO::isPointOver(int x, int y)
{
    if (!show) return false;
    return (getWindowUnderPoint(x, y) != NULL);
}


xioWindow * XIO::getWindowByName(string name)
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end(); i++)
    {
        if ((*i)->getWindowTitle() == name) return (*i);
    }
    return NULL;
}

void XIO::setElementData(xioBaseElement * element, xioEventArgs e)
{
    element->xioData = e;
    element->updateValues();
}

void XIO::setElementData(int id, xioEventArgs e)
{
    xioBaseElement * element = getElementById(id);
    if (element != NULL) setElementData(element, e);
}

xioBaseElement * XIO::getElementById(int id)
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end(); i++)
    {
        xioBaseElement * e = (*i)->getElementById(id);
        if (e != NULL) return e;
    }
    return NULL;
}

xioBaseElement * XIO::getElementByName(string name)
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end(); i++)
    {
        xioBaseElement * e = (*i)->getElementByName(name);
        if (e != NULL) return e;
    }
    return NULL;
}

xioBaseElement * XIO::getElementUnderPoint(float x, float y)
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end(); i++)
    {
        xioBaseElement * e = (*i)->elementUnderPoint(x, y);
        if (e != NULL) return e;
    }
    return NULL;
}

void XIO::setStyle(xioBaseStyle * newStyle)
{
    if (style != NULL) delete style;
    style = newStyle;
}

xioWindow * XIO::getWindowUnderPoint(int x, int y)
{
    for (vector<xioWindow*>::iterator i = windows.end() - 1; i != windows.begin() - 1; i--)
    {
        if ((*i)->isPointOverWindow(x, y)) return (*i);
    }
    return NULL;
}

void XIO::save(string filename)
{
    xml->clear();
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end(); i++)
    {
        int windowId = xml->addTag("window");
        xml->pushTag("window", windowId);
            (*i)->saveElements(xml);
        xml->popTag();
    }
    xml->saveFile(filename);
}

void XIO::load(string filename)
{
    xml->clear();
    if (xml->loadFile(filename))
    {
        if (xml->getNumTags("window") != windows.size()) return;

        for (int i = 0; i < windows.size(); i++)
        {
            xml->pushTag("window", i);
                windows[i]->loadElements(xml);
            xml->popTag();
        }
    }
    else printf("%s file does not exists!!\n", filename.c_str());
}

void XIO::bringToFront(xioWindow * window)
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end();)
    {
        if ((*i) == window) i = windows.erase(i);
        else i++;
    }
    windows.push_back(window);
}

xioWindow * XIO::getOverlappingWindow(xioWindow * window)
{
    for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end();i++)
    {
        if ((*i) != window)
        {
            if ((*i)->isPointOverWindow(window->posX, window->posY)) return (*i);
            if ((*i)->isPointOverWindow(window->posX + window->width, window->posY)) return (*i);
            if ((*i)->isPointOverWindow(window->posX + window->width, window->posY + window->height)) return (*i);
            if ((*i)->isPointOverWindow(window->posX, window->posY + window->height)) return (*i);

            if (window->isPointOverWindow((*i)->posX, (*i)->posY)) return (*i);
            if (window->isPointOverWindow((*i)->posX + (*i)->width, (*i)->posY)) return (*i);
            if (window->isPointOverWindow((*i)->posX + (*i)->width, (*i)->posY + (*i)->height)) return (*i);
            if (window->isPointOverWindow((*i)->posX, (*i)->posY + (*i)->height)) return (*i);
        }
    }
    return NULL;
}

void XIO::keyPressed(ofKeyEventArgs &e)
{
    bool doInteraction = true;
    if (alertWindow)
        if (alertWindow->isOn) doInteraction = false;

    if (show && doInteraction)
    {
        /*if (e.key == 'c')
        {
            xioBaseElement * element = getElementUnderPoint(mouse.x, mouse.y);

            if (connecting == NULL) connecting = element;
            else if (element != NULL)
            {
                connecting->connect(element);
                connecting = NULL;
            }
        }*/
        if (e.key == 13 && useKeyboardNavigation)
        {

            if (selectedWindow == NULL)
            {
                if (windows.size() == 0) return;

                selectedWindow = *(windows.end() - 1);
            }

            for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end();i++)
            {
                (*i)->setSelected((*i) == selectedWindow);
            }
        }

        for (vector<xioWindow*>::iterator i = windows.begin(); i != windows.end();i++)
        {
            (*i)->keyPressed(e);
        }
    }
}

void XIO::mouseMoved(ofMouseEventArgs & e)
{
    mouse.set(e.x, e.y);

    bool doInteraction = true;
    if (alertWindow)
        if (alertWindow->isOn) doInteraction = false;

    if (show && doInteraction)
    {
        xioWindow * prevWindowSelected = windowSelected;
        windowSelected = getWindowUnderPoint(e.x, e.y);
        if (windowSelected == NULL && prevWindowSelected != NULL) prevWindowSelected->mouseMovedOut();

        for (vector<xioWindow*>::iterator i = windows.end() - 1; i != windows.begin() - 1; i--)
        {
            if ((*i) == windowSelected) (*i)->mouseMoved(e);
        }
    }
}

void XIO::mousePressed(ofMouseEventArgs & e)
{
    mouse.set(e.x, e.y);

    bool doInteraction = true;
    if (alertWindow)
        if (alertWindow->isOn) doInteraction = false;

    if (show && doInteraction)
    {
        windowSelected = getWindowUnderPoint(e.x, e.y);

        if (windowSelected != NULL)
        {
            windowSelected->mousePressed(e);
            if (windowSelected->movable && windowSelected->isPointOverWindowTitle(e.x, e.y) && windowDragging == NULL)
            {
                bringToFront(windowSelected);

                if (windowSelected->movable)
                {
                    windowDragging = windowSelected;
                    windowSelected->windowDragPoint.set(windowSelected->posX - e.x, windowSelected->posY - e.y);
                }
            }
        }
    }
}

void XIO::mouseDragged(ofMouseEventArgs & e)
{
    mouse.set(e.x, e.y);

    bool doInteraction = true;
    if (alertWindow)
        if (alertWindow->isOn) doInteraction = false;

    if (show && doInteraction)
    {
        if (windowDragging != NULL)
        {
            windowDragging->posX = e.x + windowDragging->windowDragPoint.x;
            windowDragging->posY = e.y + windowDragging->windowDragPoint.y;
            windowDragging->collideScreenBorders();

            if (windowCollision)
            {
                xioWindow * over = getOverlappingWindow(windowDragging);
                if (over != NULL)
                {
                    if (over->posX < windowDragging->posX)
                    {
                        int dstX = over->posX + over->width;
                        if (dstX + windowDragging->width <= screenArea.x + screenArea.width)
                            windowDragging->posX = dstX;
                        else windowDragging->posX = over->posX - windowDragging->width;
                    }
                    else
                    {
                        int dstX = over->posX - windowDragging->width;
                        if (dstX >= 0)
                            windowDragging->posX = dstX;
                        else windowDragging->posX = over->posX + over->width;
                    }
                }
            }
        }
        else
        {
            if (windowSelected != NULL)
            {
                windowSelected->mouseDragged(e);
            }
        }
    }
}

void XIO::mouseReleased(ofMouseEventArgs & e)
{
    bool doInteraction = true;
    if (alertWindow)
        if (alertWindow->isOn) doInteraction = false;

    if (show && doInteraction)
    {
        if (windowSelected != NULL)
        {
            if (getWindowUnderPoint(e.x, e.y) == NULL) windowSelected->mouseMovedOut();
            windowSelected->mouseReleased(e);
        }
    }
    windowDragging = NULL;
}
