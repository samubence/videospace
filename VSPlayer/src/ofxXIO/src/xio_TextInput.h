#ifndef __xio_TextInput_h__
#define __xio_TextInput_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_TextInput : public xioBaseElement
{
    public:
        xio_TextInput(string name)
        {
            xioData.name = name;
            xioData.text = "";
            height = 40;
            active = false;
            stringToWrite = NULL;
        };

        xio_TextInput(string name, string * _stringToWrite)
        {
            xioData.name = name;
            xioData.text = "";
            height = 40;
            active = false;
            stringToWrite = _stringToWrite;
            xioData.text = *stringToWrite;
            tmpText = xioData.text;
            //updateValues();
            notifyWhenLoaded = true;
        };

        ~xio_TextInput(){};

        void draw()
        {
            parent->useElementFontColor();
            parent->drawString(xioData.name, 0, 0, width, 18, XIO_ALIGN_CENTER);
            parent->useElementStrokeColor();
            ofNoFill();
            ofRect(5, 18, width - 10, 18);
            parent->useElementFontColor();
            parent->drawString(tmpText, 5, 18, width - 10, 18, XIO_ALIGN_LEFT);

        };

        void drawBackground(bool over)
        {
            parent->useElementBgColor(over && !active);
            ofFill();
            ofRect(0, 0, width, height);
            parent->useElementStrokeColor();
            ofNoFill();
            ofRect(0, 0, width, height);
            parent->useElementBgColor(active);
            ofFill();
            ofRect(5, 18, width - 10, 18);
        };

        void fitText()
        {
            int boxW = width - 10;
            int textW = parent->getStringWidth(xioData.text);
            if (textW < boxW) tmpText = xioData.text;
            else
            {
                int length = xioData.text.length();
                string tText;
                for (int n = 1; n < length; n++)
                {
                    tmpText = tText;
                    tText = xioData.text.substr(length - n, n);
                    textW = parent->getStringWidth(tText);
                    if (textW >= boxW) break;
                }
            }
        };

        void mousePressed(float x, float y, int b)
        {
            active = !active;
            if (!active) notifyEvent();
        };

        void keyPressed(char key)
        {
            if (active)
            {
                if (key == OF_KEY_BACKSPACE)   // backgspace
                {
                    xioData.text = xioData.text.substr(0, xioData.text.length() - 1);
                    fitText();
                }
                else if (key == OF_KEY_RETURN)  // ENTER
                {
                    active = false;
                    notifyEvent();
                }
                else if ((key >= 'a' && key <= 'z') || (key >= 'A' && key <= 'Z') || (key >= '0' && key <= '9') || key == ' ' || key == '.')
                {
                    xioData.text.push_back((char)key);
                    fitText();

                }
                //printf("%s\n", xioData.text.c_str());
            }
            if (stringToWrite) (*stringToWrite) = xioData.text;
        };

        void updateValues()
        {
            tmpText = xioData.text;
            if (stringToWrite) *stringToWrite = xioData.text;
        };

        string * stringToWrite;
        string tmpText;
        bool active;

};

#endif

