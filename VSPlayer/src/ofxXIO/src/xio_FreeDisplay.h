#ifndef __xio_FreeDisplay_h__
#define __xio_FreeDisplay_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_FreeDisplay : public xioBaseElement
{
    public:
        xio_FreeDisplay(string _name, void (*_function)(float, float), float _heightRatio)
        {
            xioData.name = _name;
            function = _function;
            heightRatio = _heightRatio;
            notifyWhenLoaded = false;
        };
        ~xio_FreeDisplay();

        void draw()
        {
            height = width * heightRatio;
            (*function)(width, height);
        }

        void (*function)(float, float);
        float heightRatio;
};

#endif

