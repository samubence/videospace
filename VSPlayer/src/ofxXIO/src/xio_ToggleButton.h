#ifndef __xio_ToggleButton_h__
#define __xio_ToggleButton_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_ToggleButton : public xioBaseElement
{
    public:
        xio_ToggleButton(string _name, bool defaultState)
        {
            xioData.name = _name;
            xioData.on = defaultState;
            valuePt = NULL;
        };

        xio_ToggleButton(string _name, bool * _valuePt)
        {
            xioData.name = _name;
            xioData.on = *_valuePt;
            valuePt = _valuePt;
        };

        ~xio_ToggleButton();

        void updateValues()
        {
            if (valuePt != NULL) *valuePt = xioData.on;
        }
        void draw()
        {
            parent->useElementFontColor();
            parent->drawString(xioData.name, 0, 0, width - 10, height, XIO_ALIGN_RIGHT);

            int h = height - 8;
            if (xioData.on)
            {
                parent->useElementFillColor();
                ofFill();
                ofRect(4, 4, h, h);
            }
            parent->useElementStrokeColor();
            ofNoFill();
            ofRect(4, 4, h, h);

        }

        void mousePressed(float x, float y, int button)
        {
            xioData.on = !xioData.on;
            if (valuePt != NULL) *valuePt = xioData.on;
            notifyEvent();
        };

        void keyPressed(char key)
        {
            if (selected)
            {
                if (key == 102)
                {
                    xioData.on = true;
                    notifyEvent();
                    updateValues();
                }
                if (key == 100)
                {
                    xioData.on = false;
                    notifyEvent();
                    updateValues();
                }
            }
        }

        bool * valuePt;
};

#endif

