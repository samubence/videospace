#ifndef __xioWindow_h__
#define __xioWindow_h__

#include "ofMain.h"
#include "xioDefinitions.h"
#include "XIO.h"
#include "xioListener.h"
#include "xioBaseElement.h"
#include "ofxXmlSettings.h"

class XIO;
class xioBaseElement;
class xioListener;

class xioWindow
{
    public:
        xioWindow(string windowTitle, int posX, int posY, int width, int height, bool _movable = true, bool _isOpen = true, bool _autoHeight = false);
        ~xioWindow();

        void setMovable(bool _movable) {movable = _movable;};
        void setOpen(bool _isOpen);
        void setAutoHeight(bool _autoHeight) {autoHeight = _autoHeight;};
        void setUseMinimize(bool _useMinimize) {useMinimize = _useMinimize;};
        void setUseAutoMinimize(bool _useAutoMinimize);
        void addGroup(string groupName);
        void addElement(xioListener * listenerClass, xioBaseElement * element, int id = -1);
        void addElement(xioBaseElement * element, int id = -1);
        xioBaseElement * getElementById(int id);
        xioBaseElement * getElementByName(string name);

        void setParent(XIO * _parent) {parent = _parent;};

        void update();
        void draw();

        void mouseMoved(ofMouseEventArgs & e);
        void mousePressed(ofMouseEventArgs & e);
        void mouseDragged(ofMouseEventArgs & e);
        void mouseReleased(ofMouseEventArgs & e);
        void mouseMovedOut();
        void keyPressed(ofKeyEventArgs &e);

        void saveElements(ofxXmlSettings * xml);
        void loadElements(ofxXmlSettings * xml);

        void toggle();
        int getWidth() {return width;};
        int getHeight() {return height;};
        string getWindowTitle() {return windowTitle;};
        void collideScreenBorders();

        void calculateElementPositions();
        bool isPointOverRect(int x, int y, ofRectangle rect);
        bool isPointOverWindow(int x, int y);
        bool isPointOverWindowTitle(int x, int y);
        bool isPointOverSlider(int x, int y);
        bool isPointOverMinimize(int x, int y);

        void setSelected(bool _selected) {selected = _selected;};

        xioBaseElement * elementUnderPoint(int x, int y);

        XIO * parent;

        vector<xioBaseElement * > elements;

        // selection + dragging

        xioBaseElement * elementSelected;
        xioBaseElement * elementDragging;
        xioBaseElement * prevElementSelected;
        ofPoint windowDragPoint;

        bool displaySlider;
        int sliderY;
        bool sliderDragging;
        float sliderPercent;
        int mouseX, mouseY;
        int pmouseX, pmouseY;
        int menuContentHeight;

        // window properties
        string windowTitle;
        int posX, posY;
        int width, height;
        int originalHeight;
        bool movable;
        bool autoHeight;
        // window minimize animation hack
        float t_height, tmpHeight;
        float slideDamp;
        bool isOpen;
        bool overMinimize;
        bool useMinimize;
        bool useAutoMinimize;

        // stencil functions
        void clearStencil();
        void drawStencilMask();
        void drawStencilColor();
        void stencilOff();

        // connectors
        xioBaseElement * connecting;

        // keyboard navigation
        bool selected;
        xioBaseElement * selectedElement;
        vector<xioBaseElement*>::iterator selectedIterator;

};

#endif

