#ifndef __XIO_h__
#define __XIO_h__

#include "ofMain.h"
#include "xioWindow.h"
#include "xioAlertWindow.h"
#include "xioBaseStyle.h"
#include "xioListener.h"
#include "xioEventArgs.h"
#include "xioDefaultStyle.h"

#include "ofxXmlSettings.h"

class xioWindow;
class xioBaseElement;

class XIO
{
    public:

        XIO();
        ~XIO();

        void update(ofEventArgs &e);
        void update();
        void draw();
        void drawConnectors();

        xioWindow * createWindow(string windowTitle, int posX, int posY, int width, int height, bool movable = true, bool isOpen = true, bool autoHeight = false);

        xioAlertWindow * createAlertWindow(xioListener * listenerClass);

        void destroyWindow(xioWindow * w);
        xioWindow * getWindowByName(string name);
        xioBaseElement * getElementById(int id);
        xioBaseElement * getElementByName(string name);
        xioBaseElement * getElementUnderPoint(float x, float y);

        void setElementData(xioBaseElement * element, xioEventArgs e);
        void setElementData(int id, xioEventArgs e);
        xioWindow * getOverlappingWindow(xioWindow * window);

        void keyPressed(ofKeyEventArgs &e);
        void mouseMoved(ofMouseEventArgs & e);
        void mousePressed(ofMouseEventArgs & e);
        void mouseDragged(ofMouseEventArgs & e);
        void mouseReleased(ofMouseEventArgs & e);

        void load(string filename);
        void save(string filename);
        void setVisibility(bool _show) {show = _show; };
        void setUseKeyboardNavigation(bool use) {useKeyboardNavigation = use;};

        bool isMouseOver();
        bool isPointOver(int x, int y);

        void toggleVisibility(){show = !show; };

        void setStyle(xioBaseStyle * newStyle);
        void setScreenArea(ofRectangle _screenArea) {screenArea = _screenArea;};
        void setUseWindowCollision(bool _windowCollision) {windowCollision = _windowCollision; };
        void setUseStencil(bool _useStencil){useStencil = _useStencil;};
        void bringToFront(xioWindow * window);

        // styling functions - if no style has specified use the default one
        void drawString(string text, int x, int y, int width, int height, int alignMode) {style->drawString(text, x, y, width, height, alignMode); };

        void useWindowBgColor(bool over = false) {style->windowBg.useColor(over);};
        void useWindowStrokeColor(bool over = false) {style->windowStroke.useColor(over);};

        void useWindowTitleBgColor(bool over = false) {style->windowTitleBg.useColor(over);};
        void useWindowTitleStrokeColor(bool over = false) {style->windowTitleStroke.useColor(over);};
        void useWindowTitleFontColor(bool over = false) {style->windowTitleFontColor.useColor(over);};

        void useGroupBgColor(bool over = false) {style->groupBg.useColor(over);};
        void useGroupStrokeColor(bool over = false) {style->groupStroke.useColor(over);};
        void useGroupFontColor(bool over = false) {style->groupFontColor.useColor(over);};

        void useElementBgColor(bool over = false) {style->elementBg.useColor(over);};
        void useElementFillColor(bool over = false) {style->elementFill.useColor(over);};
        void useElementStrokeColor(bool over = false) {style->elementStroke.useColor(over);};
        void useElementFontColor(bool over = false) {style->elementFontColor.useColor(over);};

        int getStringWidth(string s) {return style->font.stringWidth(s);};
        int getStringHeight(string s) {return style->font.stringHeight(s);};

        ofRectangle screenArea;         // screen area for window screen collision

        bool useStencil;
        bool show;                      // show or hide all XIO gui + enable/disable mouse events
        bool windowCollision;
        bool useKeyboardNavigation;     // use ENTER, LEFT, RIGHT, UP, DOWN to navigate menu
        xioWindow * getWindowUnderPoint(int x, int y);

        xioBaseStyle * style;
        vector<xioWindow*> windows;
        xioWindow * windowDragging;
        xioWindow * windowSelected;

        xioAlertWindow * alertWindow;

        ofxXmlSettings * xml;

        ofPoint mouse;

        xioBaseElement * connecting;

        xioWindow * selectedWindow;


};

#endif

