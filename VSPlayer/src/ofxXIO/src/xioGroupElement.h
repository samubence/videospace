#ifndef __xioGroupElement_h__
#define __xioGroupElement_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xioGroupElement : public xioBaseElement
{
    public:
        xioGroupElement(string _name)
        {
            xioData.name = _name;
            height = XIO_DEF::XIO_GROUP_HEIGHT;
            notifyWhenLoaded = false;
        };

        ~xioGroupElement();

        void draw()
        {
            parent->useGroupFontColor();
            parent->drawString(xioData.name, 0, 0, getWidth(), getHeight(), XIO_ALIGN_CENTER);
        }

        void keyPressed(char key)
        {
            if (selected)
            {

                if (key == 102)
                {
                    isOpen = true;
                }
                if (key == 100)
                {
                    isOpen = false;
                }

            }
        }
};

#endif

