#ifndef __xioBaseStyle_h__
#define __xioBaseStyle_h__

#include "ofMain.h"

enum
{
    XIO_ALIGN_LEFT,
    XIO_ALIGN_CENTER,
    XIO_ALIGN_RIGHT
};

class xioColor
{
    public:
        xioColor()
        {
            over_a = -1;
        };
        void set(int _r, int _g, int _b, int _a = 255) {r = _r; g = _g; b = _b; a = _a;};
        void setOver(int _r, int _g, int _b, int _a = 255) {over_r = _r; over_g = _g; over_b = _b; over_a = _a;};
        void useColor(bool over = false)
        {
            if (over && over_a != -1) ofSetColor(over_r, over_g, over_b, over_a);
            else ofSetColor(r, g, b, a);
        };
        int r, g, b, a;
        int over_r, over_g, over_b, over_a;
};

class xioBaseStyle
{
    public:
        xioBaseStyle(){};
        ~xioBaseStyle(){};

        ofTrueTypeFont font;
        int fontSize;

        void drawString(string text, int x, int y, int width, int height, int align)
        {
            int w = font.stringWidth(text);
            int h = font.stringHeight(text);
            if (align == XIO_ALIGN_LEFT) font.drawString(text, (int)(x + 2), (int)(y + height / 2.f - h / 2.f + fontSize + 2));
            else if(align == XIO_ALIGN_CENTER) font.drawString(text, (int)(x + width / 2.f - w / 2.f), (int)(y + height / 2.f - h / 2.f + fontSize + 0));
            else if (align == XIO_ALIGN_RIGHT) font.drawString(text, (int)(x + width - w), (int)(y + height / 2.f - h / 2.f + fontSize + 2));
        };

        xioColor windowBg, windowStroke;
        xioColor windowTitleBg, windowTitleStroke, windowTitleFontColor;
        xioColor groupBg, groupStroke, groupFontColor;
        xioColor elementBg, elementFill, elementStroke, elementFontColor;

};

#endif

