#ifndef __xio_ImageViewer_h__
#define __xio_ImageViewer_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_ImageViewer : public xioBaseElement
{
    public:
        xio_ImageViewer(string _name, ofTexture * _img)
        {
            xioData.name = _name;
            img = _img;
            height = XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT;
            notifyWhenLoaded = false;
        };

        void draw()
        {
            if (img != NULL)
            {
                if (img->getWidth() == 0 || img->getHeight() == 0) height = XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT;
                else height = width * (img->getHeight() / (float)img->getWidth());
                ofSetColor(255, 255, 255);
                img->draw(0, 0, width, height);
            }
        };

        ofTexture * img;
};

#endif

