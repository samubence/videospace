#ifndef _XIO_DEF_
#define _XIO_DEF_

#define XIO_GROUP_ELEMENT -1
#define XIO_ELEMENT_NOP (-987654321.123456789f)

class XIO_DEF
{
    public:
        static int XIO_ELEMENT_DEFAULT_HEIGHT;
        static int XIO_WINDOW_TITLE_HEIGHT;
        static int XIO_SLIDER_WIDTH;
        static int XIO_SLIDER_HEIGHT;
        static int XIO_GROUP_HEIGHT;
};

#endif
