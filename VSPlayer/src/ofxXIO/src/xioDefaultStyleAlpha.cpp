#ifndef __XIO_DEFAULT_STYLE__
#define __XIO_DEFAULT_STYLE__

#include "xioDefaultStyleAlpha.h"
#include "xioDefinitions.h"

xioDefaultStyleAlpha::xioDefaultStyleAlpha()
{
    XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT = 20;
    XIO_DEF::XIO_WINDOW_TITLE_HEIGHT = 25;
    XIO_DEF::XIO_SLIDER_WIDTH = 25;
    XIO_DEF::XIO_SLIDER_HEIGHT = 15;
    XIO_DEF::XIO_GROUP_HEIGHT = 15;

    fontSize = 8;
    string fontName = "verdana.ttf";
    font.loadFont(fontName, fontSize, false, false);
    if (!font.isLoaded())
    {
        printf("%s NOT FOUND!!!\n", fontName.c_str());
        OF_EXIT_APP(0);
    }
    // window
    windowBg.set(100, 100, 100, 150);
    windowStroke.set(0, 0, 0, 200);

    windowTitleBg.set(100, 100, 100, 100);
    windowTitleBg.setOver(200, 200, 200, 200);
    windowTitleStroke.set(0, 200, 200, 100);
    windowTitleFontColor.set(255, 255, 255);

    // group
    groupBg.set(50, 50, 50, 100);
    groupBg.setOver(0, 100, 100, 200);
    groupStroke.set(0, 50, 50, 200);
    groupFontColor.set(0, 255, 255);

    // element
    elementBg.set(150, 250, 250, 100);
    elementBg.setOver(200, 255, 255, 120);
    elementFill.set(150, 222, 222, 200);
    elementStroke.set(0, 200, 200, 200);
    elementFontColor.set(0, 0, 0);
}

#endif
