#ifndef __xioListener_h__
#define __xioListener_h__

#include "ofMain.h"
#include "xioEventArgs.h"

class xioListener
{
    public:

        virtual void xioEvent(xioEventArgs &e){};

};

#endif

