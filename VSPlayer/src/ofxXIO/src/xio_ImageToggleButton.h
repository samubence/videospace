#ifndef __xio_ImageToggleButton_h__
#define __xio_ImageToggleButton_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_ImageToggleButton : public xioBaseElement
{
    public:
        xio_ImageToggleButton(string imageFileName, bool defaultState)
        {
            image.loadImage(imageFileName);
            xioData.on = defaultState;
            imageRatio = image.height / (float)image.width;
        };

        ~xio_ImageToggleButton();

        void draw()
        {
            height = imageRatio * width;
            xioData.on ? ofSetColor(255, 255, 255) : ofSetColor(100, 100, 100);
            image.draw(0, 0, width, height);
        }

        void mousePressed(float x, float y, int button)
        {
            xioData.on = !xioData.on;
            notifyEvent();
        };

        float imageRatio;
        ofImage image;
};

#endif

