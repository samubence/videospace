#ifndef __XIO_DEFAULT_STYLE__
#define __XIO_DEFAULT_STYLE__

#include "xioDefaultStyle.h"
#include "xioDefinitions.h"

xioDefaultStyle::xioDefaultStyle()
{
    XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT = 30;
    XIO_DEF::XIO_WINDOW_TITLE_HEIGHT = 20;
    XIO_DEF::XIO_SLIDER_WIDTH = 25;
    XIO_DEF::XIO_SLIDER_HEIGHT = 25;
    XIO_DEF::XIO_GROUP_HEIGHT = 20;

    fontSize = 14;
    string fontName = "verdana.ttf";
    font.loadFont(fontName, fontSize, true, true);
    if (!font.isLoaded())
    {
        printf("%s NOT FOUND!!!\n", fontName.c_str());
        OF_EXIT_APP(0);
    }
    // window
    windowBg.set(100, 100, 100);
    windowStroke.set(0, 0, 0);

    windowTitleBg.set(100, 100, 100);
    windowTitleBg.setOver(200, 200, 200);
    windowTitleStroke.set(0, 0, 0);
    windowTitleFontColor.set(255, 255, 255);

    // group
    groupBg.set(50, 50, 50);
    groupBg.setOver(0, 100, 100);
    groupStroke.set(0, 50, 50);
    groupFontColor.set(0, 255, 255);

    // element
    elementBg.set(150, 250, 250);
    elementBg.setOver(200, 255, 255);
    elementFill.set(150, 222, 222);
    elementStroke.set(0, 200, 200);
    elementFontColor.set(0, 128, 128);
}

#endif
