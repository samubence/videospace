#include "XIO.h"
#include "xioWindow.h"
#include "xioGroupElement.h"


xioWindow::xioWindow(string _windowTitle, int _posX, int _posY, int _width, int _height, bool _movable, bool _isOpen, bool _autoHeight)
{
    windowTitle = _windowTitle;
    posX = _posX;
    posY = _posY;
    width = _width;
    height = originalHeight = _height;
    movable = _movable;
    autoHeight = _autoHeight;

    displaySlider = false;
    sliderDragging = false;
    sliderPercent = 0;

    menuContentHeight = 0;

    elementSelected = NULL;
    elementDragging = NULL;
    prevElementSelected = NULL;
    connecting = NULL;
    selectedElement = NULL;
    selected = true;

    // minimize hack
    useAutoMinimize = false;
    useMinimize = true;
    overMinimize = false;
    isOpen = _isOpen;
    t_height = height;                                 // target height
    if (!isOpen) tmpHeight = XIO_DEF::XIO_WINDOW_TITLE_HEIGHT; // current height (float)
    else tmpHeight = height;
    slideDamp = 3.f;

}

xioWindow::~xioWindow()
{

}

void xioWindow::addGroup(string groupName)
{
    addElement(NULL, new xioGroupElement(groupName), XIO_GROUP_ELEMENT);
}

void xioWindow::addElement(xioListener * listenerClass, xioBaseElement * element, int id)
{
    element->setParent(parent);
    element->setListener(listenerClass);
    element->setId(id);
    element->width = width;
    element->setup();
    elements.push_back(element);
}

void xioWindow::addElement(xioBaseElement * element, int id)
{
    element->setParent(parent);
    element->setId(id);
    element->width = width;
    element->setup();
    elements.push_back(element);
}

void xioWindow::saveElements(ofxXmlSettings * xml)
{
    for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
    {
        int elementId = xml->addTag("element");
        xml->pushTag("element", elementId);
            (*i)->saveElementData(xml);
        xml->popTag();
    }
}

xioBaseElement * xioWindow::getElementById(int id)
{
    for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
    {
        if ((*i)->xioData.id == id) return (*i);
    }
    return NULL;
}

xioBaseElement * xioWindow::getElementByName(string name)
{
    for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
    {
        //if ((*i)->xioData.name.compare(name) != 0) return (*i);
        if ((*i)->xioData.name == name) return (*i);
    }
    return NULL;
}

void xioWindow::loadElements(ofxXmlSettings * xml)
{
    if (xml->getNumTags("element") != elements.size()) return;
    for (int i = 0; i < elements.size(); i++)
    {
        xml->pushTag("element", i);
            elements[i]->loadElementData(xml);
        xml->popTag();
    }
    /*
    int n = xml->getNumTags("element");
    for (int i = 0; i < n; i++)
    {
        xml->pushTag("element", i);
            string name = xml->getValue("name", "");

            xioBaseElement * e = getElementByName(name);
            //printf("%s :: %i\n", name.c_str(), e);
            if (e)
                e->loadElementData(xml);
            //elements[i]->loadElementData(xml);
        xml->popTag();
    }
    */
}

void xioWindow::calculateElementPositions()
{
    displaySlider = ((menuContentHeight - (height - XIO_DEF::XIO_WINDOW_TITLE_HEIGHT)) > 0);    // use slider only if the content bigger than the window

    if (!displaySlider)                         // if no slider, set slider to zero
    {
        sliderPercent = 0;
        sliderY = 0;
    }

    int y = 0;

    bool display = true;                        // variable for closed gruops

    y += XIO_DEF::XIO_WINDOW_TITLE_HEIGHT;

    y -= sliderPercent * (menuContentHeight - (height - XIO_DEF::XIO_WINDOW_TITLE_HEIGHT));     // use the slider

    menuContentHeight = 0;                      // summ the height of the elements, even if they are out of the window

    if (parent->useStencil)
    {
        for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
        {
            if (display || (*i)->isGroupElement())                                                              // if the element is in an open group or it is an element
            {
                if (y <= height)                                                                                // if it if it`s over the bottom edge dont display it and dont increse the "y"
                {
                    if (y >= XIO_DEF::XIO_WINDOW_TITLE_HEIGHT - (*i)->getHeight())                                       // if it`s overt to top edge, dont display it, but add it to the "y"
                    {
                        (*i)->setAbsolutePosition(posX, posY + y, width - XIO_DEF::XIO_SLIDER_WIDTH, (*i)->getHeight()); // set the element position
                        (*i)->onScreen = true;                                                                  // set to be displayable
                    }
                    else (*i)->onScreen = false;                                                                // if its over the top edge dont display

                    y += (*i)->getHeight();                                                                     // but add to the "y"
                }
                else (*i)->onScreen = false;                                                                    // if its ovet the bottom, dont add to the "y"

                menuContentHeight += (*i)->getHeight();                                                         //summ the elements height
            }
            else (*i)->onScreen = false;                                                                        // if its onside a closed group dont display

            if ((*i)->isGroupElement()) display = ((*i)->isOpen);                                               // switch "disply"
        }
    }
    else                                                                                                        // if not using stencil buffer dont draw overlaping elements
    {
        for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
        {
            if (display || (*i)->isGroupElement())
            {
                if (y + (*i)->getHeight() <= height)
                {
                    if (y >= XIO_DEF::XIO_WINDOW_TITLE_HEIGHT)
                    {
                        (*i)->setAbsolutePosition(posX, posY + y, width - XIO_DEF::XIO_SLIDER_WIDTH, (*i)->getHeight());
                        (*i)->onScreen = true;
                    }
                    else (*i)->onScreen = false;

                    y += (*i)->getHeight();
                }
                else (*i)->onScreen = false;

                menuContentHeight += (*i)->getHeight();
            }
            else (*i)->onScreen = false;

            if ((*i)->isGroupElement()) display = ((*i)->isOpen);
        }
    }

    if (autoHeight) t_height = min(menuContentHeight + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, (int)(parent->screenArea.y + parent->screenArea.height));
}

void xioWindow::update()
{
    // slide effect
    if (!isOpen) tmpHeight += (XIO_DEF::XIO_WINDOW_TITLE_HEIGHT - tmpHeight) / slideDamp;
    else tmpHeight += (t_height - tmpHeight) / slideDamp;
    if (fabsf(tmpHeight - t_height) < 1) tmpHeight = t_height;
    height = (int)tmpHeight;

    // calc element/group list
    calculateElementPositions();

    // update elements
    for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
    {
        (*i)->baseUpdate();
    }
}

void xioWindow::draw()
{

    // draw window background
    parent->useWindowBgColor();

    ofFill();
    ofRect(posX, posY, width, height);

    // draw elements
    if (parent->useStencil)
    {
        clearStencil();
        drawStencilMask();
        ofFill();
        ofRect(posX, posY + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, width - XIO_DEF::XIO_SLIDER_WIDTH, height - XIO_DEF::XIO_WINDOW_TITLE_HEIGHT);
        drawStencilColor();
    }

    for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
    {
        (*i)->baseDraw( elementSelected == (*i) );
    }
    if (parent->useStencil) stencilOff();

    // draw window frame
    parent->useWindowStrokeColor();
    ofNoFill();
    glLineWidth(3);
    ofRect(posX, posY, width, height);
    glLineWidth(1);

    // draw title
    parent->useWindowTitleStrokeColor();
    ofLine(posX, posY + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, posX + width - XIO_DEF::XIO_SLIDER_WIDTH, posY + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT);
    ofLine(posX + width - XIO_DEF::XIO_SLIDER_WIDTH, posY + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, posX + width - XIO_DEF::XIO_SLIDER_WIDTH, posY + height);
    parent->useWindowTitleFontColor();
    parent->drawString(windowTitle, posX, posY, width, XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, XIO_ALIGN_CENTER);

    // minimize button
    if (useMinimize)
    {
        parent->useWindowTitleStrokeColor();
        ofNoFill();
        int size = XIO_DEF::XIO_WINDOW_TITLE_HEIGHT - 10;
        glLineWidth(2);
        ofRect(posX + width - size - 5, posY + 5, size, size);
        glLineWidth(1);
        if (overMinimize) ofFill();
        if (!isOpen) ofTriangle(posX + width - size - 5, posY + 5, posX + width - size / 2 - 5, posY + size + 5, posX + width - 5, posY + 5);
        else ofTriangle(posX + width - size - 5, posY + size + 5, posX + width - size / 2 - 5, posY + 5, posX + width - 5, posY + size + 5);
    }

    // draw slider if needed
    if (displaySlider && isOpen)
    {
        parent->useWindowTitleBgColor();
        ofFill();
        glLineWidth(3);
        ofRect(posX + width - XIO_DEF::XIO_SLIDER_WIDTH, sliderY + posY + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, XIO_DEF::XIO_SLIDER_WIDTH, XIO_DEF::XIO_SLIDER_HEIGHT);
        parent->useWindowTitleStrokeColor();
        ofNoFill();
        ofRect(posX + width - XIO_DEF::XIO_SLIDER_WIDTH, sliderY + posY + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, XIO_DEF::XIO_SLIDER_WIDTH, XIO_DEF::XIO_SLIDER_HEIGHT);
        glLineWidth(1);
    }

}

void xioWindow::setOpen(bool _isOpen)
{
    isOpen = _isOpen;
    if (!isOpen) tmpHeight = XIO_DEF::XIO_WINDOW_TITLE_HEIGHT;
    else tmpHeight = originalHeight;
}

void xioWindow::setUseAutoMinimize(bool _useAutoMinimize)
{
    useAutoMinimize = _useAutoMinimize;
    setUseMinimize(!useAutoMinimize);
}

bool xioWindow::isPointOverWindow(int x, int y)
{
    return isPointOverRect(x, y, ofRectangle(posX, posY, width, height));
}

bool xioWindow::isPointOverWindowTitle(int x, int y)
{
    return isPointOverRect(x, y, ofRectangle(posX, posY, width, XIO_DEF::XIO_WINDOW_TITLE_HEIGHT)) && !isPointOverMinimize(x, y);
}

bool xioWindow::isPointOverSlider(int x, int y)
{
    return isPointOverRect(x, y, ofRectangle(posX + width - XIO_DEF::XIO_SLIDER_WIDTH, posY + XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, XIO_DEF::XIO_SLIDER_WIDTH, height - XIO_DEF::XIO_SLIDER_HEIGHT)) && displaySlider;
}

xioBaseElement * xioWindow::elementUnderPoint(int x, int y)
{
    for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
    {
        if ((*i)->isOnScreen())
            if (isPointOverRect(x, y, (*i)->getBoundingBox()))
            {
                return (*i);
            }
    }
    return NULL;
}

bool xioWindow::isPointOverMinimize(int x, int y)
{
    int size = XIO_DEF::XIO_WINDOW_TITLE_HEIGHT;
    return isPointOverRect(x, y, ofRectangle(posX + width - size, posY, size, size)) && useMinimize;
}

bool xioWindow::isPointOverRect(int x, int y, ofRectangle rect)
{
    return (x >= rect.x && x <= rect.x + rect.width && y >= rect.y && y <= rect.y + rect.height);
}

void xioWindow::collideScreenBorders()
{
    if (posX < parent->screenArea.x) posX = parent->screenArea.x;
    if (posY < parent->screenArea.y) posY = parent->screenArea.y;
    if (posX > parent->screenArea.x + parent->screenArea.width - width) posX = parent->screenArea.x + parent->screenArea.width - width;
    if (posY > parent->screenArea.y + parent->screenArea.height - height) posY = parent->screenArea.y + parent->screenArea.height - height;
}

void xioWindow::mouseMoved(ofMouseEventArgs & e)
{
    mouseX = e.x;
    mouseY = e.y;

    if (useAutoMinimize) isOpen = true;

    overMinimize = isPointOverMinimize(e.x, e.y);

    elementSelected = elementUnderPoint(e.x, e.y);

    if (elementSelected != NULL)
    {
        float x = ofClamp((e.x - elementSelected->posX) / (float)(elementSelected->width), 0, 1);
        float y = ofClamp((e.y - elementSelected->posY) / (float)(elementSelected->height), 0, 1);

        elementSelected->mouseMoved(x, y);
    }
    pmouseX = e.x;
    pmouseY = e.y;

    if (prevElementSelected != elementSelected && prevElementSelected != NULL)
    {
        prevElementSelected->mouseMovedOut();
    }

    prevElementSelected = elementSelected;
}

void xioWindow::keyPressed(ofKeyEventArgs &e)
{
    if (selected)
    {
        /*if (e.key == 13)
        {
            if (selectedElement == NULL)
            {
                if (elements.size() > 0)
                {
                    selectedIterator = elements.begin();
                    selectedElement = *(selectedIterator);
                }
            }
        }*/
/*
        if (e.key == OF_KEY_DOWN)
        {
            if (selectedElement->myListenerClass == NULL && !selectedElement->isOpen && selectedIterator < elements.end() - 1)
            {
                for (vector<xioBaseElement*>::iterator i = selectedIterator + 1; i != elements.end(); i++)
                {
                    if ((*i)->myListenerClass == NULL) {selectedIterator = i;break;}
                }
            }
            else
            {
                selectedIterator++;
                if (selectedIterator >= elements.end())
                {
                    selectedIterator = elements.begin();
                }
            }
            selectedElement = (*selectedIterator);
        }
        if (e.key == OF_KEY_UP)
        {
            if (selectedElement->myListenerClass == NULL && selectedIterator != elements.begin())   // if it is a group, find the previoous one
            {
                for (vector<xioBaseElement*>::iterator i = selectedIterator - 1; i != elements.begin() - 1; i--)
                {
                    if ((*i)->myListenerClass == NULL)
                    {
                        if  (!(*i)->isOpen) selectedIterator = i;
                        else
                        {
                            selectedIterator--;
                        }
                        break;
                    }
                }
            }
            else
            {
                if (selectedIterator != elements.begin()) selectedIterator--;
            }
            selectedElement = (*selectedIterator);
        }*/

        for (vector<xioBaseElement * >::iterator i = elements.begin(); i != elements.end(); i++)
        {

            if (selected) (*i)->setSelected((*i) == selectedElement);
            else (*i)->setSelected(false);

            (*i)->keyPressed(e.key);
        }
    }
}

void xioWindow::mousePressed(ofMouseEventArgs & e)
{
    mouseX = e.x;
    mouseY = e.y;

    if (isPointOverMinimize(e.x, e.y)) isOpen = !isOpen;

    if (isOpen)
    {
        elementSelected = elementUnderPoint(e.x, e.y);

        if (elementSelected != NULL)
        {
            if (elementSelected->isGroupElement())
            {
                elementSelected->toggleGroup();
            }
            else
            {
                elementDragging = elementSelected;

                float x = ofClamp((e.x - elementDragging->posX) / (float)(elementDragging->width), 0, 1);
                float y = ofClamp((e.y - elementDragging->posY) / (float)(elementDragging->height), 0, 1);

                elementDragging->mousePressed(x, y, e.button);
                //elementDragging->mousePressed(x, y);
                //elementDragging->mousePressed();
            }
        }
        else
        {
            sliderDragging = isPointOverSlider(e.x, e.y);
        }
    }

    pmouseX = e.x;
    pmouseY = e.y;
}

void xioWindow::mouseDragged(ofMouseEventArgs & e)
{
    mouseX = e.x;
    mouseY = e.y;

    if (elementDragging != NULL)
    {
        float x = ofClamp((e.x - elementDragging->posX) / (float)(elementDragging->width), 0, 1);
        float y = ofClamp((e.y - elementDragging->posY) / (float)(elementDragging->height), 0, 1);

        elementDragging->mouseDragged(x, y, e.button);
    }
    else if (sliderDragging)
    {
        sliderY += (e.y - pmouseY);
        if (sliderY < 0) sliderY = 0;
        else if (sliderY > height - XIO_DEF::XIO_SLIDER_HEIGHT - XIO_DEF::XIO_WINDOW_TITLE_HEIGHT) sliderY = height - XIO_DEF::XIO_SLIDER_HEIGHT - XIO_DEF::XIO_WINDOW_TITLE_HEIGHT;

        sliderPercent = ofMap(sliderY, 0, height - XIO_DEF::XIO_SLIDER_HEIGHT - XIO_DEF::XIO_WINDOW_TITLE_HEIGHT, 0, 1);
    }

    pmouseX = e.x;
    pmouseY = e.y;
}

void xioWindow::mouseMovedOut()
{
    if (prevElementSelected != NULL)
    {
        prevElementSelected->mouseMovedOut();
    }

    if (useAutoMinimize) isOpen = false;
    overMinimize = false;
    prevElementSelected = NULL;
    elementSelected = NULL;
    elementDragging = NULL;
    sliderDragging = false;
}

void xioWindow::mouseReleased(ofMouseEventArgs & e)
{
    if (elementDragging != NULL)
    {
        float x = ofClamp((e.x - elementDragging->posX) / (float)(elementDragging->width), 0, 1);
        float y = ofClamp((e.y - elementDragging->posY) / (float)(elementDragging->height), 0, 1);

        elementDragging->mouseReleased(x, y, e.button);
    }
    elementSelected = NULL;
    elementDragging = NULL;
    sliderDragging = false;
}

void xioWindow::clearStencil()
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_STENCIL_BUFFER_BIT);
}

void xioWindow::drawStencilMask()
{

    glEnable(GL_STENCIL_TEST);
    glColorMask(0, 0, 0, 0);
    glStencilFunc(GL_ALWAYS, 1, 1);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

}

void xioWindow::drawStencilColor()
{
    glColorMask(1, 1, 1, 1);
    glStencilFunc(GL_EQUAL, 1, 1);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
}

void xioWindow::stencilOff()
{
    glDisable(GL_STENCIL_TEST);
}
