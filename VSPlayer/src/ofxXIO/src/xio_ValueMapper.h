#ifndef __xio_ValueMapper_h__
#define __xio_ValueMapper_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_ValueMapper : public xioBaseElement
{
    public:
        xio_ValueMapper(string _name, float _valueMin, float _valueMax, float defaultValue)
        {
            xioData.name = _name;
            valueMin = _valueMin;
            valueMax = _valueMax;
            valuePercent = ofMap(defaultValue, valueMin, valueMax, 0, 1);
            xioData.value = defaultValue;
            thresholdMin = 0.4;//valueMin;
            thresholdMax = 0.8;//valueMax;
            dragged = &thresholdMax;
        };
        ~xio_ValueMapper();

        void draw()
        {
            float x = valuePercent * getWidth();

            parent->useElementFillColor();
            ofFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementStrokeColor();
            ofNoFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementFontColor();
            ofFill();
            ofRect(thresholdMin * width, 0, 3, getHeight());
            ofRect(thresholdMax * width, 0, 3, getHeight());


            parent->useElementFontColor();

            parent->drawString(xioData.name + " : " + ofToString(xioData.value, 4), 0, 0, getWidth(), getHeight(), XIO_ALIGN_CENTER);
        };

        void mousePressed(float x, float y, int button)
        {
            if (button == 2)
            {
                if (fabsf(x - thresholdMin) < fabsf(x - thresholdMax))
                    dragged = &thresholdMin;
                else
                    dragged = &thresholdMax;
                *dragged = x;
            }
            else
            {
                valuePercent = x;
                xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            }

            notifyEvent();
            updateValues();
        };

        void updateValues()
        {
            valuePercent = ofMap(xioData.value, valueMin, valueMax, 0, 1, true);
        };

        void updateConnection(xioBaseElement * element)
        {
            element->xioData.value = ofMap(xioData.value, thresholdMin, thresholdMax, 0, 1);
            element->updateValues();
            element->notifyEvent();
            printf("%s %f\n", xioData.name.c_str(), xioData.value);
        }

        void mouseDragged(float x, float y, int button)
        {
            if (button == 2)
            {
                *dragged = x;
                if (thresholdMax < thresholdMin) thresholdMin = thresholdMax - 0.0001;
                if (thresholdMin > thresholdMax) thresholdMax = thresholdMin + 0.0001;

                thresholdMin = ofClamp(thresholdMin, valueMin, valueMax);
                thresholdMax = ofClamp(thresholdMax, valueMin, valueMax);
            }
            else
            {
                valuePercent = x;
                xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            }

            notifyEvent();
            updateValues();
        };

        void saveExtraData(ofxXmlSettings * xml)
        {
            xml->setValue("thresholdMin", thresholdMin);
            xml->setValue("thresholdMax", thresholdMax);
        }

        void loadExtraData(ofxXmlSettings * xml)
        {
            thresholdMin = xml->getValue("thresholdMin", 0.f);
            thresholdMax = xml->getValue("thresholdMax", 1.f);
        }

        float valuePercent;
        float valueMin, valueMax;
        float thresholdMin, thresholdMax;

        float * dragged;
};

#endif

