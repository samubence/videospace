//
//  xio_NumberInput.h
//  VideoSpace
//
//  Created by bence samu on 26/01/14.
//
//

#ifndef VideoSpace_xio_NumberInput_h
#define VideoSpace_xio_NumberInput_h

#include "xioBaseElement.h"

class xio_NumberInput : public xioBaseElement
{
public:

    xio_NumberInput(string name, float _minValue, float _maxValue, float * _floatPtr)
    {
        xioData.name = name;
        xioData.text = "";
        valueMin = _minValue;
        valueMax = _maxValue;
        //height = 25;
        active = false;
        notifyWhenLoaded = true;
        floatPtr = _floatPtr;
        intPtr = NULL;
        xioData.value = *floatPtr;
        xioData.text = ofToString(xioData.value);
        tmpText = xioData.text;
        blink = false;
    };
    
    xio_NumberInput(string name, float _minValue, float _maxValue, int * _intPtr)
    {
        xioData.name = name;
        xioData.text = "";
        valueMin = _minValue;
        valueMax = _maxValue;
        //height = 25;
        active = false;
        notifyWhenLoaded = true;
        floatPtr = NULL;
        intPtr = _intPtr;
        xioData.value = *intPtr;
        xioData.text = ofToString(xioData.value);
        tmpText = xioData.text;
        blink = false;
    };

    xio_NumberInput(string name, float _minValue, float _maxValue, float _value)
    {
        xioData.name = name;
        xioData.text = "";
        xioData.value = _value;
        valueMin = _minValue;
        valueMax = _maxValue;
        //height = 25;
        active = false;
        notifyWhenLoaded = true;
        floatPtr = NULL;
        intPtr = NULL;
        //xioData.value = *floatPtr;
        xioData.text = ofToString(xioData.value);
        tmpText = xioData.text;
        blink = false;
    };

    xio_NumberInput(){};

    void draw()
    {
        if (ofGetFrameNum() % 10 == 0)
            blink = !blink;
        
        parent->useElementStrokeColor();
        ofFill();
        ofRect(5, 5, width - 10, 15);
        float x = ofMap(xioData.value, valueMin, valueMax, 0, width - 10, true);
        
        parent->useElementFillColor();
        ofFill();
        ofRect(5, 5, x, 15);
        parent->useElementStrokeColor();
        ofNoFill();
        ofRect(5, 5, x, 15);
        parent->useElementFontColor();
        parent->drawString(tmpText + ((blink && active) ? ":" : ""), 5, 5, width - 10, 15, XIO_ALIGN_LEFT);
        parent->drawString(xioData.name, 0, 5, width - 10, 15, XIO_ALIGN_RIGHT);
    };

    void drawBackground(bool over)
    {
        parent->useElementBgColor(over && !active);
        ofFill();
        ofRect(0, 0, width, height);
        parent->useElementStrokeColor();
        ofNoFill();
        ofRect(0, 0, width, height);
        parent->useElementBgColor(active);
        ofFill();
        ofRect(5, 5, width - 10, 15);
    };

    void fitText()
    {
        int boxW = width - 10;
        int textW = parent->getStringWidth(xioData.text);
        if (textW < boxW) tmpText = xioData.text;
        else
        {
            int length = xioData.text.length();
            string tText;
            for (int n = 1; n < length; n++)
            {
                tmpText = tText;
                tText = xioData.text.substr(length - n, n);
                textW = parent->getStringWidth(tText);
                if (textW >= boxW) break;
            }
        }
    };

    void mousePressed(float x, float y, int b)
    {
        active = true;
    };

    void mouseDragged(float x, float y, int b)
    {
        xioData.value = ofMap(x, 0, 1, valueMin, valueMax);
        if (intPtr) xioData.value = (int)xioData.value;
        xioData.text = ofToString(xioData.value);
        fitText();
        updateValues();

        if (!active) notifyEvent();
    }

    void mouseMovedOut()
    {
        active = false;
    }
    
    void mouseReleased(float x, float y, int b)
    {
        notifyEvent();
    }

    void keyPressed(char key)
    {
        if (active)
        {
            if (key == OF_KEY_BACKSPACE)   // backgspace
            {
                xioData.text = xioData.text.substr(0, xioData.text.length() - 1);
                fitText();
            }
            else if (key == OF_KEY_RETURN)  // ENTER
            {
                active = false;
                notifyEvent();
            }
            else if ((key >= 'a' && key <= 'z') || (key >= 'A' && key <= 'Z') || (key >= '0' && key <= '9') || key == '.' || key == '-')
            {
                xioData.text.push_back((char)key);
                fitText();

            }
            //printf("%s\n", xioData.text.c_str());
        }
        xioData.value = ofToFloat(xioData.text);
        if (floatPtr) (*floatPtr) = xioData.value;
        if (intPtr) (*intPtr) = (int)xioData.value;
    };

    void updateValues()
    {
        tmpText = xioData.text;
        if (floatPtr) *floatPtr = xioData.value;
        if (intPtr) *intPtr = (int)xioData.value;
    };

    string tmpText;
    bool active;
    float * floatPtr;
    int * intPtr;
    float valueMin, valueMax;
    bool blink;
};

#endif
