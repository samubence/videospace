#ifndef __XIO_ALL_ELEMENTS__
#define __XIO_ALL_ELEMENTS__

// styles

#include "xioDefaultStyleAlpha.h"

//

#include "xio_PushButton.h"
#include "xio_Slider.h"
#include "xio_SliderList.h"
#include "xio_TextInput.h"
#include "xio_ToggleButton.h"
#include "xio_ImageButton.h"
#include "xio_ImageToggleButton.h"
#include "xio_ToggleList.h"
#include "xio_PixelViewer.h"
#include "xio_ImageViewer.h"
//#include "xio_OpenCvViewer.h"
#include "xio_FreeDisplay.h"
#include "xio_ValueMapper.h"
#include "xio_NumberInput.h"


#endif
