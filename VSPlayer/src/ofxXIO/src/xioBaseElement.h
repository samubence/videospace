#ifndef __xioBaseElement_h__
#define __xioBaseElement_h__

#include "ofMain.h"
#include "XIO.h"
#include "xioListener.h"
#include "ofxXmlSettings.h"

class XIO;

class xioBaseElement : public xioListener
{
    public:
        xioBaseElement();
        ~xioBaseElement();

        void baseUpdate();
        void baseDraw(bool over = false);

        // common functions
        void setId(int _id) {xioData.id = _id; };
        void notifyEvent();
        void setAbsolutePosition(int x, int y, int width, int height);
        void saveElementData(ofxXmlSettings * xml);
        void loadElementData(ofxXmlSettings * xml);

        // virtual functions
        virtual void saveExtraData(ofxXmlSettings * xml){};
        virtual void loadExtraData(ofxXmlSettings * xml){};
        virtual void setup(){};
        virtual void update(){};
        virtual void updateConnection(xioBaseElement * element);
        virtual void draw(){};
        virtual void drawBackground(bool over = false);

        virtual void mouseMoved(float x, float y){};
        virtual void mousePressed(float x, float y, int b){};
        virtual void mouseDragged(float x, float y, int b){};
        virtual void mouseReleased(float x, float y, int b){};
        virtual void mouseMovedOut(){};
        virtual void keyPressed(char key);
        virtual void updateValues(){};
        virtual xioEventArgs getData(){return xioData;};

        // set dependencies
        void setParent(XIO * _parent) {parent = _parent; };
        void setListener(xioListener * listenerClass);

        int getWidth() {return width;};
        int getHeight() {return height;};

        // is the element on the screen or out of the window
        bool isOnScreen() {return onScreen;};
        ofRectangle getBoundingBox() {return ofRectangle(posX, posY, width, height);};
        void setSelected(bool _selected = false) {selected = _selected;};

        // if it has no listener class it must be a group separator
        bool isGroupElement() {return (myListenerClass == NULL);};

        // toggle groupElement open/close proprty
        void toggleGroup() {if (collapsible) isOpen = !isOpen;};

        void connect(xioBaseElement * slider);
        void disconnect(xioBaseElement * slider);

        // root xio instance
        XIO * parent;

        int width, height;
        int posX, posY;

        bool notifyWhenLoaded;  // disable this for elements like push button

        bool onScreen;
        bool isOpen;    // only for group element

        // event listener style
        xioListener * myListenerClass;
        ofEvent<xioEventArgs> xioEvent;
        xioEventArgs xioData;

        vector<xioBaseElement*> connectedElements;

        bool selected;
        bool collapsible;
};

#endif

