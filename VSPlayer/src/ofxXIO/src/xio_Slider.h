#ifndef __xio_Slider_h__
#define __xio_Slider_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_Slider : public xioBaseElement
{
    public:
        xio_Slider(string _name, float _valueMin, float _valueMax, float defaultValue)
        {
            xioData.name = _name;
            valueMin = _valueMin;
            valueMax = _valueMax;
            valuePercent = ofMap(defaultValue, valueMin, valueMax, 0, 1);
            xioData.value = defaultValue;
            isFloat = true;
            floatPt = NULL;
            intPt = NULL;
        };

        xio_Slider(string _name, int _valueMin, int _valueMax, int defaultValue)
        {
            xioData.name = _name;
            valueMin = _valueMin;
            valueMax = _valueMax;
            valuePercent = ofMap(defaultValue, valueMin, valueMax, 0, 1);
            xioData.value = defaultValue;
            floatPt = NULL;
            intPt = NULL;
            isFloat = false;
        };

        xio_Slider(string _name, float _valueMin, float _valueMax, float * _valuePt)
        {
            xioData.name = _name;
            valueMin = _valueMin;
            valueMax = _valueMax;
            valuePercent = ofMap(*_valuePt, valueMin, valueMax, 0, 1);
            xioData.value = *_valuePt;
            floatPt = _valuePt;
            intPt = NULL;
            isFloat = true;
        };

        xio_Slider(string _name, float _valueMin, float _valueMax, int * _valuePt)
        {
            xioData.name = _name;
            valueMin = _valueMin;
            valueMax = _valueMax;
            valuePercent = ofMap(*_valuePt, valueMin, valueMax, 0, 1);
            xioData.value = *_valuePt;
            floatPt = NULL;
            intPt = _valuePt;
            isFloat = false;
        };

        ~xio_Slider();

        void update()
        {
            if (floatPt != NULL)  xioData.value = *floatPt;
            if (intPt != NULL) xioData.value = *intPt;
        };

        void draw()
        {

            float x = valuePercent * getWidth();

            parent->useElementFillColor();
            ofFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementStrokeColor();
            ofNoFill();
            ofRect(0, 0, x, getHeight());

            parent->useElementFontColor();

            parent->drawString(xioData.name + " : " + ofToString(xioData.value, isFloat ? 4 : 0), 0, 0, getWidth(), getHeight(), XIO_ALIGN_CENTER);
        };

        void mousePressed(float x, float y, int button)
        {
            valuePercent = x;
            xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            if (!isFloat) xioData.value = (int)xioData.value;

            notifyEvent();
            updateValues();
        };

        void updateValues()
        {
            valuePercent = ofMap(xioData.value, valueMin, valueMax, 0, 1);
            if (floatPt != NULL) *floatPt = xioData.value;
            if (intPt != NULL) *intPt = xioData.value;

        };

        void mouseDragged(float x, float y, int button)
        {
            valuePercent = x;
            xioData.value = valueMin + valuePercent * (valueMax - valueMin);
            if (!isFloat) xioData.value = (int)xioData.value;
            notifyEvent();
            updateValues();
        };

        void keyPressed(char key)
        {
            if (selected)
            {
                float step = 0.001;
                if (key == 102)
                {
                    if (!isFloat)
                    {
                        xioData.value ++;
                        if (xioData.value >= valueMax) xioData.value = valueMax;
                    }
                    else
                    {
                        valuePercent = ofClamp(valuePercent + step, 0, 1);
                        xioData.value = valueMin + valuePercent * (valueMax - valueMin);
                    }

                    notifyEvent();
                    updateValues();
                }
                if (key == 100)
                {
                    if (!isFloat)
                    {
                        xioData.value --;
                        if (xioData.value < valueMin) xioData.value = valueMin;
                    }
                    else
                    {
                        valuePercent = ofClamp(valuePercent - step, 0, 1);
                        xioData.value = valueMin + valuePercent * (valueMax - valueMin);
                    }

                    notifyEvent();
                    updateValues();
                }
            }
        }


        bool isFloat;
        float valuePercent;
        float valueMin, valueMax;

        float * floatPt;
        int * intPt;

};

#endif

