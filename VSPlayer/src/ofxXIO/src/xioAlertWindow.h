#ifndef __xioAlertWindow_h__
#define __xioAlertWindow_h__

#include "ofMain.h"
#include "xioEventArgs.h"
#include "xioListener.h"

class xioAlertWindow
{
    public:
        xioAlertWindow(xioListener * myListener);
        ~xioAlertWindow();
        void setText(string _ok, string _yes, string _no);
        void alert(string message, int _alertId, bool yesno = false);
        void draw();

        void mousePressed(ofMouseEventArgs & e);
        void mouseMoved(ofMouseEventArgs & e);
        int getStringWidth(string s) { return font.stringWidth(s); };
        int getStringHeight(string s) { return font.stringHeight(s); };
        bool isPointOverRect(int x, int y, ofRectangle rect) {return (x >= rect.x && x <= rect.x + rect.width && y >= rect.y && y <= rect.y + rect.height);};

        ofTrueTypeFont font;
        string message;
        bool yesno;
        bool isOn;
        ofRectangle AlertWindow;
        ofRectangle messageBox;
        ofRectangle yesButton;
        ofRectangle noButton;
        bool overYes, overNo;
        string ok, yes, no;
        int alertId;
        ofEvent<xioEventArgs> alertEvent;
        xioListener * myListenerClass;
};

#endif

