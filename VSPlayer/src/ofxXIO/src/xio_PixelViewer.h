#ifndef __xio_PixelViewer_h__
#define __xio_PixelViewer_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_PixelViewer : public xioBaseElement
{
    public:
        xio_PixelViewer(string _name, unsigned char * _pixels, int _resx, int _resy, ofImageType _pixelType = OF_IMAGE_COLOR)
        {
            xioData.name = _name;
            pixels = _pixels;
            resx = _resx;
            resy = _resy;
            pixelType = _pixelType;
            img = new ofImage();
            img->allocate(resx, resy, pixelType);
            notifyWhenLoaded = false;
        };

        ~xio_PixelViewer()
        {
            delete img;
        };

        void setup()
        {
            height = (int)(resy / (float)resx * width);
        };

        void draw()
        {
            if (pixels != NULL)
                img->setFromPixels(pixels, resx, resy, pixelType);

            ofSetColor(255, 255, 255);
            img->draw(0, 0, width, height);
        };

        int resx, resy;
        ofImageType pixelType;
        unsigned char * pixels;
        ofImage * img;
};

#endif

