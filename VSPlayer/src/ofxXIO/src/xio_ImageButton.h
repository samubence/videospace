#ifndef __xio_ImageButton_h__
#define __xio_ImageButton_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_ImageButton : public xioBaseElement
{
    public:
        xio_ImageButton(string imageFileName)
        {
            image.loadImage(imageFileName);
            timeToBePushed = 100;
            pressedTime = -1;
            imageRatio = image.height / (float)image.width;
            notifyWhenLoaded = false;       // dont want to notify every time xio loaded, only whwen user pushed me

        };

        ~xio_ImageButton();

        void draw()
        {
            height = imageRatio * width;

            if (pressedTime != -1 && ofGetElapsedTimeMillis() - pressedTime < timeToBePushed) ofSetColor(100, 100, 100, 100);
            else ofSetColor(255, 255, 255);
            image.draw(0, 0, width, height);

        };

        void mousePressed(float x, float y, int button)
        {
            pressedTime = ofGetElapsedTimeMillis();
            notifyEvent();
        };

        ofImage image;
        float imageRatio;
        int timeToBePushed;
        int pressedTime;

};

#endif

