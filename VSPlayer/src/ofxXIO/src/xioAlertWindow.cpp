#include "xioAlertWindow.h"

xioAlertWindow::xioAlertWindow(xioListener * myListener)
{
    myListenerClass = myListener;
    font.loadFont("verdana.ttf", 8, false, false);
    ofAddListener(ofEvents().mousePressed, this, &xioAlertWindow::mousePressed);
    ofAddListener(ofEvents().mouseMoved, this, &xioAlertWindow::mouseMoved);

    ofAddListener(alertEvent, myListenerClass, &xioListener::xioEvent);

    ok = "OK";
    yes = "YES";
    no = "NO";

    isOn = false;
}

xioAlertWindow::~xioAlertWindow()
{

}

void xioAlertWindow::alert(string _message, int _alertId, bool _yesno)
{
    message = _message;
    alertId = _alertId;
    yesno = _yesno;
    isOn = true;

    int spacer = 10;

    int buttonWidth = 60;
    int buttonHeight = 20;

    int txtW = getStringWidth(message) + 4;
    int txtH = getStringHeight(message) + 4;
    int width = max(txtW + spacer * 2, buttonWidth * 2 + spacer * 2);
    int height = txtH + buttonHeight + spacer * 3;

    AlertWindow.x = ofGetWidth() / 2. - width / 2.;
    AlertWindow.y = ofGetHeight() / 2. - height / 2.;
    AlertWindow.width = width;
    AlertWindow.height = height;

    messageBox.x = AlertWindow.x + AlertWindow.width / 2. - txtW / 2.;
    messageBox.y = AlertWindow.y + spacer;
    messageBox.width = txtW;
    messageBox.height = txtH;

    if (yesno)
    {
        yesButton.x = AlertWindow.x + AlertWindow.width / 2. - buttonWidth;
        yesButton.y = AlertWindow.y + spacer + messageBox.height + spacer;
        yesButton.width = buttonWidth;
        yesButton.height = buttonHeight;

        noButton.x = AlertWindow.x + AlertWindow.width / 2.;
        noButton.y = AlertWindow.y + spacer + messageBox.height + spacer;
        noButton.width = buttonWidth;
        noButton.height = buttonHeight;
    }
    else
    {
        yesButton.x = AlertWindow.x + AlertWindow.width / 2. - buttonWidth / 2.;
        yesButton.y = AlertWindow.y + spacer + messageBox.height + spacer;
        yesButton.width = buttonWidth;
        yesButton.height = buttonHeight;
    }
}

void xioAlertWindow::draw()
{
    if (isOn)
    {
        ofSetColor(100, 100, 100, 128);
        ofFill();
        ofRect(AlertWindow.x, AlertWindow.y, AlertWindow.width, AlertWindow.height);
        ofSetColor(255, 255, 255, 128);
        ofNoFill();
        ofRect(AlertWindow.x, AlertWindow.y, AlertWindow.width, AlertWindow.height);

        ofSetColor(255, 0, 0);
        ofFill();
        ofRect(messageBox.x, messageBox.y, messageBox.width, messageBox.height);
        ofSetColor(255, 255, 255);
        font.drawString(message, messageBox.x + 2, messageBox.y + font.getLineHeight());

        if (yesno)
        {
            overYes ? ofSetColor(255, 0, 0) : ofSetColor(50, 50, 50);
            ofFill();
            ofRect(yesButton.x, yesButton.y, yesButton.width, yesButton.height);
            overNo ? ofSetColor(255, 0, 0) : ofSetColor(50, 50, 50);
            ofRect(noButton.x, noButton.y, noButton.width, noButton.height);
            ofSetColor(250, 250, 250);
            ofNoFill();
            ofRect(yesButton.x, yesButton.y, yesButton.width, yesButton.height);
            ofRect(noButton.x, noButton.y, noButton.width, noButton.height);

            font.drawString(yes, yesButton.x + yesButton.width / 2. - getStringWidth(yes) / 2., yesButton.y + font.getLineHeight());
            font.drawString(no, noButton.x + noButton.width / 2. - getStringWidth(no) / 2., noButton.y + font.getLineHeight());
        }
        else
        {
            overYes ? ofSetColor(255, 0, 0) : ofSetColor(50, 50, 50);
            ofFill();
            ofRect(yesButton.x, yesButton.y, yesButton.width, yesButton.height);

            ofSetColor(250, 250, 250);
            ofNoFill();
            ofRect(yesButton.x, yesButton.y, yesButton.width, yesButton.height);
            font.drawString(ok, yesButton.x + yesButton.width / 2. - getStringWidth(ok) / 2., yesButton.y + font.getLineHeight());
        }
    }
}

void xioAlertWindow::setText(string _ok, string _yes, string _no)
{
    ok = _ok;
    yes = _yes;
    no = _no;
}

void xioAlertWindow::mouseMoved(ofMouseEventArgs &e)
{
    overYes = isPointOverRect(e.x, e.y, yesButton);
    overNo = isPointOverRect(e.x, e.y, noButton);
}

void xioAlertWindow::mousePressed(ofMouseEventArgs &e)
{
    if (isOn)
    {
        if (isPointOverRect(e.x, e.y, yesButton))
        {
            xioEventArgs ae;
            ae.id = alertId;
            ae.text = message;
            ae.on = true;
            isOn = false;
            ofNotifyEvent(alertEvent, ae, myListenerClass);
        }
        else if (isPointOverRect(e.x, e.y, noButton))
        {
            xioEventArgs ae;
            ae.id = alertId;
            ae.text = message;
            ae.on = false;
            isOn = false;
            ofNotifyEvent(alertEvent, ae, myListenerClass);
        }
    }
}

