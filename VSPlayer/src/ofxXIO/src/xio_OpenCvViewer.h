#ifndef __xio_OpenCVViewer_h__
#define __xio_OpenCVViewer_h__

#include "ofMain.h"
#include "xioBaseElement.h"
#include "ofxCvImage.h"

class xio_OpenCvViewer : public xioBaseElement
{
    public:
        xio_OpenCvViewer(string _name, ofxCvImage * _img)
        {
            xioData.name = _name;
            img = _img;
            notifyWhenLoaded = false;
        };
        ~xio_OpenCvViewer();

        void draw()
        {
            if (img != NULL)
            {
                height = img->height / (float)img->width * width;
                ofSetColor(255, 255, 255);
                img->draw(0, 0, width, height);
            }
        };

        ofxCvImage * img;
};

#endif

