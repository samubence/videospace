#ifndef __xio_PushButton_h__
#define __xio_PushButton_h__

#include "ofMain.h"
#include "xioBaseElement.h"

class xio_PushButton : public xioBaseElement
{
    public:
        xio_PushButton(string _name)
        {
            xioData.name = _name;
            timeToBePushed = 100;
            pressedTime = -1;
            notifyWhenLoaded = false;       // dont want to notify every time xio loaded, only whwen user pushed me

        };

        ~xio_PushButton();

        void draw()
        {
            parent->useElementFontColor();
            parent->drawString(xioData.name, 0, 0, width, height, XIO_ALIGN_CENTER);
            int w = parent->getStringWidth(xioData.name) + 10;
            int h = parent->getStringHeight(xioData.name) + 8;

            if (pressedTime != -1 && ofGetElapsedTimeMillis() - pressedTime < timeToBePushed)
            {
                parent->useElementFillColor();
                ofFill();
                ofRect((width - w) / 2., (height - h) / 2., w, h);
            }
            parent->useElementStrokeColor();
            ofNoFill();
            ofRect((width - w) / 2., (height - h) / 2., w, h);
        };

        void mousePressed(float x, float y, int button)
        {
            pressedTime = ofGetElapsedTimeMillis();
            notifyEvent();
        };

        void keyPressed(char key)
        {
            if (selected)
            {
                if (key == 102)
                {
                    pressedTime = ofGetElapsedTimeMillis();
                    notifyEvent();
                }

            }
        }

        int timeToBePushed;
        int pressedTime;
};

#endif

