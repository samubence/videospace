#include "xioBaseElement.h"
#include "xioDefinitions.h"

xioBaseElement::xioBaseElement()
{
    myListenerClass = NULL;
    width = 0;
    height = XIO_DEF::XIO_ELEMENT_DEFAULT_HEIGHT;
    // clear xioData.
    xioData.id = XIO_GROUP_ELEMENT; // default is group elelemtn
    xioData.name = "";
    xioData.value = XIO_ELEMENT_NOP;
    xioData.on = XIO_ELEMENT_NOP;
    xioData.text = "";

    notifyWhenLoaded = true;
    posX = posY = 0;
    onScreen = false;
    isOpen = true;
    selected = false;
    collapsible = true;
}

xioBaseElement::~xioBaseElement()
{

}

void xioBaseElement::saveElementData(ofxXmlSettings * xml)
{
    xml->setValue("id", xioData.id);
    if (xioData.id == XIO_GROUP_ELEMENT) xml->setValue("open", isOpen);
    if (xioData.name != "") xml->setValue("name", xioData.name);
    if (xioData.value != XIO_ELEMENT_NOP) xml->setValue("value", xioData.value);
    if (xioData.on != XIO_ELEMENT_NOP) xml->setValue("on", xioData.on);
    if (xioData.text != "") xml->setValue("text", xioData.text);
    if (xioData.values.size() > 0)
    {
        int vectorId = xml->addTag("vectorValues");
        xml->pushTag("vectorValues", vectorId);
            for (int i = 0; i < xioData.values.size(); i++)
            {
                xml->setValue("vector", xioData.values[i], i);
            }
        xml->popTag();
    }

    saveExtraData(xml);
}

void xioBaseElement::loadElementData(ofxXmlSettings * xml)
{
    xioEventArgs tmp;
    tmp = xioData;

    //xioData.id = xml->getValue("id", XIO_GROUP_ELEMENT);
    if (xioData.id == XIO_GROUP_ELEMENT) isOpen = xml->getValue("open", 1);
    //xioData.name = xml->getValue("name", "");
    xioData.value = xml->getValue("value", XIO_ELEMENT_NOP);
    xioData.on = xml->getValue("on", XIO_ELEMENT_NOP);
    xioData.text = xml->getValue("text", "");
    if (xml->tagExists("vectorValues"))
    {
        xioData.values.clear();
        xml->pushTag("vectorValues");
        int n = xml->getNumTags("vector");
        for (int i =0; i < n; i++)
        {
            xioData.values.push_back(xml->getValue("vector", XIO_ELEMENT_NOP, i));
        }
        xml->popTag();
    }

    loadExtraData(xml);

    if (notifyWhenLoaded)
    {
        //printf("%s\n", xioData.name.c_str());
        updateValues();
        notifyEvent();
    }
}



void xioBaseElement::notifyEvent()
{
    if (myListenerClass != NULL)
        ofNotifyEvent(xioEvent, xioData, myListenerClass);
    else
        ofNotifyEvent(xioEvent, xioData);

    for (int i = 0; i < connectedElements.size(); i++)
    {
        updateConnection(connectedElements[i]);
    }
}

void xioBaseElement::updateConnection(xioBaseElement * element)
{
    element->xioData.value = xioData.value;
    element->updateValues();
    element->notifyEvent();
}

void xioBaseElement::baseUpdate()
{
    update();
}

void xioBaseElement::connect(xioBaseElement * element)
{
    connectedElements.push_back(element);
}

void xioBaseElement::disconnect(xioBaseElement * element)
{
    for (vector<xioBaseElement*>::iterator i = connectedElements.begin(); i != connectedElements.end();)
    {
        if (element == (*i))
        {
            delete *i;
            i = connectedElements.erase(i);
        }
        else i++;
    }
}

void xioBaseElement::drawBackground(bool over)
{
    glLineWidth(1);
    if (isGroupElement()) // if I`m a group element use different style
    {
        parent->useGroupBgColor(over);
        ofFill();
        ofRect(0, 0, width, height);
        parent->useGroupStrokeColor();
        ofNoFill();
        ofRect(0, 0, width, height);
    }
    else
    {
        parent->useElementBgColor(over);
        ofFill();
        ofRect(0, 0, width, height);
        parent->useElementStrokeColor();
        ofNoFill();
        ofRect(0, 0, width, height);
    }
}

void xioBaseElement::baseDraw(bool over)
{
    if (onScreen)
    {
        glLineWidth(1);
        glPushMatrix();
            glTranslatef(posX, posY, 0);

            drawBackground(over || selected);
            draw();

        glPopMatrix();
    }
}

void xioBaseElement::setListener(xioListener * listenerClass)
{
    myListenerClass = listenerClass;
    ofAddListener(xioEvent, myListenerClass, &xioListener::xioEvent);
}

void xioBaseElement::setAbsolutePosition(int x, int y, int _width, int _height)
{
    posX = x;
    posY = y;
    width = _width;
    height = _height;
}

void xioBaseElement::keyPressed(char key)
{

}
