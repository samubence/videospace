#ifndef __xioEventArgs_h__
#define __xioEventArgs_h__

#include "ofMain.h"

class xioEventArgs
{
    public:
        xioEventArgs()
        {
            id = -1;
            name = "";
            value = -1;
            on = -1;
            text = "";
        };
        ~xioEventArgs(){};

        float getFloat() {return value; };
        int getInt() {return (int)value; };
        string getString() {return text; };
        bool getBool() {return on; };
        vector<float> getVector() {return values; };

        // -------------

        int id;
        string name;
        float value;
        vector<float> values;
        float on;
        string text;
};

#endif
