#ifndef __SceneContent_h__
#define __SceneContent_h__

#include "ofMain.h"
#include "ofApp.h"

class ContentPlyer
{
public:
    virtual ~ContentPlyer(){};
    virtual void setup(string fileName, ofVec3f _center, float _width, float _height, ofVec3f rotation){};
    virtual void update(){};
    virtual ofTexture * getTexture() {return NULL;};
    virtual void play(bool looping = false){};
    virtual void setPercent(float p){};
    virtual int getNumFrames(){return 0;};
    virtual void setSpeed(float v){};
    virtual void setFPS(int _fps){};
    virtual void stop(){};
    virtual void draw(ofRectangle view){ofRect(view);};
    virtual void baseDraw()
    {
        ofPushMatrix();
        ofTranslate(center.x, center.y, center.z);
        ofRotate(rotation.z, 0, 0, 1);
        ofRotate(rotation.y, 0, 1, 0);
        ofRotate(rotation.x, 1, 0, 0);

        ofRectangle view(-width / 2., - height / 2., width, height);
        draw(view);

        ofPopMatrix();
    };

    ofVec3f center, rotation;
    float width, height;
    string fileName;
    int type;
};

class ContentMoviePlayer : public ContentPlyer
{
public:
    void setup(string _fileName, ofVec3f _center, float _width, float _height, ofVec3f _rotation);
    ~ContentMoviePlayer();

    void play(bool _looping = false);

    void stop();

    void update();

    void setPercent(float p);

    ofTexture * getTexture();

    void setSpeed(float v);

    void draw(ofRectangle view);

    ofVideoPlayer vPlayer;
};

class ContentSeqPlayer : public ContentPlyer
{
public:
    void setup(string dirName, ofVec3f _center, float _width, float _height, ofVec3f _rotation);

    ~ContentSeqPlayer();

    void play(bool _looping = false);

    void setSpeed(float v);

    void setFPS(int _fps);

    void update();

    void setPercent(float p);

    ofTexture * getTexture();

    void draw(ofRectangle view);

    int getNumFrames();

    bool playing, looping;
    float fps;
    int currentFrame;
    long started;
    vector<ofTexture*> frames;
};

class NiArea
{
public:
    enum
    {
        CONTENT_PLAY,
        CONTENT_LOOP,
        CONTENT_X_2_PERCENT,
        CONTENT_Y_2_PERCENT,
        CONTENT_Z_2_PERCENT
    };

    NiArea(){};


    void setup(ofRectangle _area, ContentPlyer * _cPlayer, int _type, bool _hideWhenInactive, int _fadeInterval)
    {
        area = ofRectangle(_area.x, _area.y, _area.width, _area.height);
        cPlayer = _cPlayer;
        pType = -1;
        type = _type;
        hideWhenInactive = _hideWhenInactive;
        active = false;
        fadeStarted = -1;
        fadePercent = 0;
        fadeInterval = _fadeInterval;
        userPos.set(-1, -1);
        border = 200;
        fadeUp = false;
    };

    void updateUserPos(ofVec3f p)
    {
        if (pType != type)
        {
            if (pType == CONTENT_LOOP && type == CONTENT_PLAY)
            {
                cPlayer->play(false);
            }
            if (pType == CONTENT_PLAY && type == CONTENT_LOOP)
            {
                cPlayer->play(true);
            }
            if (pType < 2 && type >= 2)
            {
                if (type == CONTENT_X_2_PERCENT)
                    cPlayer->setPercent(1 - ofMap(p.x, area.x, area.x + area.width, 0, 1, true));
                else if (type == CONTENT_Y_2_PERCENT)
                    cPlayer->setPercent(1 - ofMap(p.y, 0, area.height, 0, 1, true));
                else if (type == CONTENT_Z_2_PERCENT)
                    cPlayer->setPercent(1 - ofMap(p.z, area.y, area.y + area.height, 0, 1, true));
            }
            pType = type;
        }
        if (userPos != p)
        {
            userPos = p;
            if (ofRectangle(area.x, area.y, area.width + 1, area.height + 1).inside(p.x, p.z))
            {
                if (!active && fadeStarted == -1)
                {
                    //printf("IN\n");
                    if (type == CONTENT_LOOP)
                        cPlayer->play(true);
                    else if (type == CONTENT_PLAY)
                        cPlayer->play(false);
                    if (fadeInterval > 0)
                    {
                        fadeStarted = ofGetElapsedTimeMillis();
                        fadeUp = true;
                    }
                    else
                    {
                        active = true;
                        fadePercent = 1;
                    }
                }
                //active = true;
            }
            else
            {
                if (active && fadeStarted == -1)
                {
                    //printf("OUT\n");
                    if (fadeInterval > 0)
                    {
                        fadeStarted = ofGetElapsedTimeMillis();
                        fadeUp = false;
                    }
                    else
                    {
                        active = false;
                        fadePercent = 0;
                    }
                    if (/*hideWhenInactive && */fadeInterval == 0)
                    {
                        cPlayer->stop();
                    }
                }
                //active = false;
            }

            if (active)
            {
                if (type == CONTENT_X_2_PERCENT)
                    cPlayer->setPercent(ofMap(p.x, area.x, area.x + area.width, 0, 1, true));
                else if (type == CONTENT_Y_2_PERCENT)
                    cPlayer->setPercent(1 - ofMap(p.y, 0, area.height, 0, 1, true));
                else if (type == CONTENT_Z_2_PERCENT)
                    cPlayer->setPercent(1 - ofMap(p.z, area.y, area.y + area.height, 0, 1, true));
            }
        }

        if (fadeStarted != -1)
        {

            if (fadeUp)
            {
                fadePercent = powf(ofMap(ofGetElapsedTimeMillis(), fadeStarted, fadeStarted + fadeInterval, 0, 1, true), .2);
                if (fadePercent == 1) fadeStarted = -1;
            }
            else
            {
                fadePercent = powf(ofMap(ofGetElapsedTimeMillis(), fadeStarted, fadeStarted + fadeInterval, 1, 0, true), .2);
                if (fadePercent == 0)
                {
                    fadeStarted = -1;
                    cPlayer->stop();
                }
            }
            active = fadePercent > 0;
        }

    };

    void draw()
    {
        if (active || !hideWhenInactive)
        {
            hideWhenInactive ? ofSetColor(255, fadePercent * 255) : ofSetColor(255);
            cPlayer->baseDraw();
        }
    }

    void drawArea(bool selected)
    {

        selected ? ofSetColor(0, 255, 255, 100) : ofSetColor(255, fadePercent * 50);
        ofFill();
        ofRect(area);

        ofSetColor(255, 50);
        ofNoFill();
        ofRect(area);
    }


    ContentPlyer * cPlayer;
    ofVec3f userPos;
    ofRectangle area;
    int type, pType;
    bool active;
    bool hideWhenInactive;
    long fadeStarted;
    int fadeInterval;
    bool fadeUp;
    float fadePercent;
    float border;
};

class SceneContent
{
public:
    enum
    {
        CONTENT_MOVIE,
        CONTENT_SEQUENCE
    };

    SceneContent(string fileName, int _contentType, ofVec3f center, float width, float height, ofVec3f rotation, ofRectangle _area, int type, bool hide, int fadeInterval, float speed)
    {
        contentType = _contentType;

        if (contentType == CONTENT_MOVIE)
            cPlayer = new ContentMoviePlayer();
        else if (contentType == CONTENT_SEQUENCE)
            cPlayer = new ContentSeqPlayer();

        cPlayer->setup(fileName, center, width, height, rotation);
        cPlayer->setSpeed(speed);

        area = new NiArea();
        area->setup(_area, cPlayer, type, hide, fadeInterval);
    };

    ~SceneContent()
    {
        delete cPlayer;
        delete area;
    };

    void update(ofVec3f userPos)
    {
        area->updateUserPos(userPos);
        cPlayer->update();

    };

    void draw()
    {
        ofSetColor(255);
        area->draw();
    };

    void drawArea(bool selected)
    {
        area->drawArea(selected);
    };

    ContentPlyer * cPlayer;
    NiArea * area;
    int contentType;
};

#endif
