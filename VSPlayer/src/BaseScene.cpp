//
//  BaseScene.cpp
//  VSPlayer
//
//  Created by bence samu on 25/01/15.
//
//

#include "BaseScene.h"

BaseScene::BaseScene()
{
    float sceneSize = 2000;
    scene.set(-sceneSize / 2., 0, sceneSize, sceneSize);
    userPos.set(0, 0, sceneSize);
    cam.disableMouseInput();
    cam.setNearClip(1);
    cam.setFarClip(4000);
    setupCamera(0, 0, 0, 0, 620, 620, 60);
};
BaseScene::~BaseScene()
{
    for (vector<SceneContent*>::iterator i = content.begin(); i != content.end(); )
    {
        //printf("deleting scene: %i\n", i);
        delete *i;
        i = content.erase(i);
    }
};


void BaseScene::setupCamera(float minX, float maxX, float minY, float maxY, float minZ, float maxZ, float fov)
{
    camMinX = minX;
    camMaxX = maxX;
    camMinY = minY;
    camMaxY = maxY;
    camMinZ = minZ;
    camMaxZ = maxZ;
    
    camFov = fov;
}

void BaseScene::addContent(SceneContent * sc)
{
    content.push_back(sc);
}

void BaseScene::removeContent(SceneContent * sc)
{
    for (vector<SceneContent*>::iterator i = content.begin(); i != content.end(); )
    {
        if (*i == sc)
        {
            i = content.erase(i);
        }
        else i++;
    }
}

void BaseScene::updateContent()
{
    for (int i = 0; i < content.size(); i++)
    {
        content[i]->update(userPos);
    }
    
    camHead.x = ofMap(userPos.x, scene.x, scene.x + scene.width, camMinX, camMaxX, true);
    camHead.y = ofMap(userPos.y, 0, scene.height, camMinY, camMaxY, true);
    camHead.z = ofMap(userPos.z, scene.y, scene.y + scene.height, camMinZ, camMaxZ, true);
    cam.setPosition(camHead);
    cam.setTarget(camHead + ofVec3f(0, 0, -1000));
    cam.setFov(camFov);
};

void BaseScene::drawContent()
{
    vector<SceneContent*> contentToSort = content;
    std::sort(contentToSort.begin(), contentToSort.end(), compareDepth);
    
    cam.begin();
    for (int i = 0; i < contentToSort.size(); i++)
    {
        contentToSort[i]->draw();
    }
    cam.end();
};

void BaseScene::setUserPos(ofVec3f p)
{
    userPos = ofVec3f(ofClamp(p.x, scene.x + 1, scene.x + scene.width - 1), ofClamp(p.y, 0, scene.width), ofClamp(p.z, scene.y + 1, scene.y + scene.height - 1));
    userPos2d.set(ofMap(userPos.x, scene.x, scene.x + scene.width, 0, 1, true),
                  ofMap(userPos.z, scene.y, scene.y + scene.height, 0, 1, true));
};

void BaseScene::drawContentAreas(SceneContent * cContent)
{
    
    for (int i = 0; i < content.size(); i++)
    {
        content[i]->drawArea(content[i] == cContent);
    }
}

bool compareDepth(const SceneContent * a, const SceneContent * b)
{
    return a->cPlayer->center.z < b->cPlayer->center.z;
};

