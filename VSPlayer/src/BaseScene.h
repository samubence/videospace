#ifndef __BaseScene_h__
#define __BaseScene_h__

#include "ofMain.h"
#include "SceneContent.h"

class SceneContent;

class BaseScene
{
public:
    BaseScene();
    ~BaseScene();
    
    virtual void setup(){};
    virtual void update(){updateContent();};
    virtual void draw(){drawContent();};
    
    void setupCamera(float minX, float maxX, float minY, float maxY, float minZ, float maxZ, float fov);
    void addContent(SceneContent * sc);
    void removeContent(SceneContent * sc);
    void updateContent();
    void drawContent();
    void setUserPos(ofVec3f p);
    void drawContentAreas(SceneContent * cContent);
    
    ofVec3f userPos;
    ofVec2f userPos2d;
    ofRectangle scene;
    vector<SceneContent*> content;
    ofEasyCam cam;
    float camMinX, camMaxX;
    float camMinY, camMaxY;
    float camMinZ, camMaxZ;
    float camFov;
    ofVec3f camHead;
};

static bool compareDepth(const SceneContent * a, const SceneContent * b);

#endif

