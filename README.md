VideoSpace is a cross-platform project created by Bence Samu @ MOME (2014-15)

This is a simple tool for creating video based interactive projections.
The software is able to track the position of one user. The positions can be mapped to play, scratch videos in a 2D or a 3D space.

This is the first prototype, it was build using openFrameworks (v0.8.4)
